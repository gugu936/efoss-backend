package com.lawrence.efoss.web.rest;

import com.lawrence.efoss.EfossApp;
import com.lawrence.efoss.domain.DeliveryAddress;
import com.lawrence.efoss.repository.DeliveryAddressRepository;
import com.lawrence.efoss.service.DeliveryAddressService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.lawrence.efoss.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DeliveryAddressResource} REST controller.
 */
@SpringBootTest(classes = EfossApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class DeliveryAddressResourceIT {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_STREET = "AAAAAAAAAA";
    private static final String UPDATED_STREET = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private DeliveryAddressService deliveryAddressService;

    @Autowired
    private MockMvc restDeliveryAddressMockMvc;

    private DeliveryAddress deliveryAddress;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryAddress createEntity() {
        DeliveryAddress deliveryAddress = new DeliveryAddress()
            .userId(DEFAULT_USER_ID)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .phone(DEFAULT_PHONE)
            .city(DEFAULT_CITY)
            .street(DEFAULT_STREET)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return deliveryAddress;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryAddress createUpdatedEntity() {
        DeliveryAddress deliveryAddress = new DeliveryAddress()
            .userId(UPDATED_USER_ID)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .city(UPDATED_CITY)
            .street(UPDATED_STREET)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return deliveryAddress;
    }

    @BeforeEach
    public void initTest() {
        deliveryAddressRepository.deleteAll();
        deliveryAddress = createEntity();
    }

    @Test
    public void createDeliveryAddress() throws Exception {
        int databaseSizeBeforeCreate = deliveryAddressRepository.findAll().size();

        // Create the DeliveryAddress
        restDeliveryAddressMockMvc.perform(post("/api/delivery-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deliveryAddress)))
            .andExpect(status().isCreated());

        // Validate the DeliveryAddress in the database
        List<DeliveryAddress> deliveryAddressList = deliveryAddressRepository.findAll();
        assertThat(deliveryAddressList).hasSize(databaseSizeBeforeCreate + 1);
        DeliveryAddress testDeliveryAddress = deliveryAddressList.get(deliveryAddressList.size() - 1);
        assertThat(testDeliveryAddress.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testDeliveryAddress.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testDeliveryAddress.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testDeliveryAddress.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testDeliveryAddress.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testDeliveryAddress.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testDeliveryAddress.getStreet()).isEqualTo(DEFAULT_STREET);
        assertThat(testDeliveryAddress.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testDeliveryAddress.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    public void createDeliveryAddressWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = deliveryAddressRepository.findAll().size();

        // Create the DeliveryAddress with an existing ID
        deliveryAddress.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeliveryAddressMockMvc.perform(post("/api/delivery-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deliveryAddress)))
            .andExpect(status().isBadRequest());

        // Validate the DeliveryAddress in the database
        List<DeliveryAddress> deliveryAddressList = deliveryAddressRepository.findAll();
        assertThat(deliveryAddressList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllDeliveryAddresses() throws Exception {
        // Initialize the database
        deliveryAddressRepository.save(deliveryAddress);

        // Get all the deliveryAddressList
        restDeliveryAddressMockMvc.perform(get("/api/delivery-addresses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deliveryAddress.getId())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].street").value(hasItem(DEFAULT_STREET)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))));
    }
    
    @Test
    public void getDeliveryAddress() throws Exception {
        // Initialize the database
        deliveryAddressRepository.save(deliveryAddress);

        // Get the deliveryAddress
        restDeliveryAddressMockMvc.perform(get("/api/delivery-addresses/{id}", deliveryAddress.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(deliveryAddress.getId()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.street").value(DEFAULT_STREET))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    public void getNonExistingDeliveryAddress() throws Exception {
        // Get the deliveryAddress
        restDeliveryAddressMockMvc.perform(get("/api/delivery-addresses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateDeliveryAddress() throws Exception {
        // Initialize the database
        deliveryAddressService.save(deliveryAddress);

        int databaseSizeBeforeUpdate = deliveryAddressRepository.findAll().size();

        // Update the deliveryAddress
        DeliveryAddress updatedDeliveryAddress = deliveryAddressRepository.findById(deliveryAddress.getId()).get();
        updatedDeliveryAddress
            .userId(UPDATED_USER_ID)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .city(UPDATED_CITY)
            .street(UPDATED_STREET)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        restDeliveryAddressMockMvc.perform(put("/api/delivery-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedDeliveryAddress)))
            .andExpect(status().isOk());

        // Validate the DeliveryAddress in the database
        List<DeliveryAddress> deliveryAddressList = deliveryAddressRepository.findAll();
        assertThat(deliveryAddressList).hasSize(databaseSizeBeforeUpdate);
        DeliveryAddress testDeliveryAddress = deliveryAddressList.get(deliveryAddressList.size() - 1);
        assertThat(testDeliveryAddress.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testDeliveryAddress.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testDeliveryAddress.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testDeliveryAddress.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testDeliveryAddress.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testDeliveryAddress.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testDeliveryAddress.getStreet()).isEqualTo(UPDATED_STREET);
        assertThat(testDeliveryAddress.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDeliveryAddress.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    public void updateNonExistingDeliveryAddress() throws Exception {
        int databaseSizeBeforeUpdate = deliveryAddressRepository.findAll().size();

        // Create the DeliveryAddress

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeliveryAddressMockMvc.perform(put("/api/delivery-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(deliveryAddress)))
            .andExpect(status().isBadRequest());

        // Validate the DeliveryAddress in the database
        List<DeliveryAddress> deliveryAddressList = deliveryAddressRepository.findAll();
        assertThat(deliveryAddressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteDeliveryAddress() throws Exception {
        // Initialize the database
        deliveryAddressService.save(deliveryAddress);

        int databaseSizeBeforeDelete = deliveryAddressRepository.findAll().size();

        // Delete the deliveryAddress
        restDeliveryAddressMockMvc.perform(delete("/api/delivery-addresses/{id}", deliveryAddress.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DeliveryAddress> deliveryAddressList = deliveryAddressRepository.findAll();
        assertThat(deliveryAddressList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
