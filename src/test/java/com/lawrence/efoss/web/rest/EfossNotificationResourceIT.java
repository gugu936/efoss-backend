package com.lawrence.efoss.web.rest;

import com.lawrence.efoss.EfossApp;
import com.lawrence.efoss.domain.EfossNotification;
import com.lawrence.efoss.repository.NotificationRepository;
import com.lawrence.efoss.service.NotificationService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.lawrence.efoss.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NotificationResource} REST controller.
 */
@SpringBootTest(classes = EfossApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class EfossNotificationResourceIT {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private MockMvc restNotificationMockMvc;

    private EfossNotification efossNotification;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EfossNotification createEntity() {
        EfossNotification efossNotification = new EfossNotification()
            .userId(DEFAULT_USER_ID)
            .message(DEFAULT_MESSAGE)
            .type(DEFAULT_TYPE)
            .status(DEFAULT_STATUS)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return efossNotification;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EfossNotification createUpdatedEntity() {
        EfossNotification efossNotification = new EfossNotification()
            .userId(UPDATED_USER_ID)
            .message(UPDATED_MESSAGE)
            .type(UPDATED_TYPE)
            .status(UPDATED_STATUS)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return efossNotification;
    }

    @BeforeEach
    public void initTest() {
        notificationRepository.deleteAll();
        efossNotification = createEntity();
    }

    @Test
    public void createNotification() throws Exception {
        int databaseSizeBeforeCreate = notificationRepository.findAll().size();

        // Create the Notification
        restNotificationMockMvc.perform(post("/api/notifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(efossNotification)))
            .andExpect(status().isCreated());

        // Validate the Notification in the database
        List<EfossNotification> efossNotificationList = notificationRepository.findAll();
        assertThat(efossNotificationList).hasSize(databaseSizeBeforeCreate + 1);
        EfossNotification testEfossNotification = efossNotificationList.get(efossNotificationList.size() - 1);
        assertThat(testEfossNotification.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testEfossNotification.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testEfossNotification.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testEfossNotification.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testEfossNotification.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testEfossNotification.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    public void createNotificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = notificationRepository.findAll().size();

        // Create the Notification with an existing ID
        efossNotification.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotificationMockMvc.perform(post("/api/notifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(efossNotification)))
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<EfossNotification> efossNotificationList = notificationRepository.findAll();
        assertThat(efossNotificationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllNotifications() throws Exception {
        // Initialize the database
        notificationRepository.save(efossNotification);

        // Get all the notificationList
        restNotificationMockMvc.perform(get("/api/notifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(efossNotification.getId())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))));
    }

    @Test
    public void getNotification() throws Exception {
        // Initialize the database
        notificationRepository.save(efossNotification);

        // Get the notification
        restNotificationMockMvc.perform(get("/api/notifications/{id}", efossNotification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(efossNotification.getId()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    public void getNonExistingNotification() throws Exception {
        // Get the notification
        restNotificationMockMvc.perform(get("/api/notifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateNotification() throws Exception {
        // Initialize the database
        notificationService.save(efossNotification);

        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

        // Update the notification
        EfossNotification updatedEfossNotification = notificationRepository.findById(efossNotification.getId()).get();
        updatedEfossNotification
            .userId(UPDATED_USER_ID)
            .message(UPDATED_MESSAGE)
            .type(UPDATED_TYPE)
            .status(UPDATED_STATUS)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        restNotificationMockMvc.perform(put("/api/notifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedEfossNotification)))
            .andExpect(status().isOk());

        // Validate the Notification in the database
        List<EfossNotification> efossNotificationList = notificationRepository.findAll();
        assertThat(efossNotificationList).hasSize(databaseSizeBeforeUpdate);
        EfossNotification testEfossNotification = efossNotificationList.get(efossNotificationList.size() - 1);
        assertThat(testEfossNotification.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testEfossNotification.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testEfossNotification.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testEfossNotification.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testEfossNotification.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testEfossNotification.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    public void updateNonExistingNotification() throws Exception {
        int databaseSizeBeforeUpdate = notificationRepository.findAll().size();

        // Create the Notification

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotificationMockMvc.perform(put("/api/notifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(efossNotification)))
            .andExpect(status().isBadRequest());

        // Validate the Notification in the database
        List<EfossNotification> efossNotificationList = notificationRepository.findAll();
        assertThat(efossNotificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteNotification() throws Exception {
        // Initialize the database
        notificationService.save(efossNotification);

        int databaseSizeBeforeDelete = notificationRepository.findAll().size();

        // Delete the notification
        restNotificationMockMvc.perform(delete("/api/notifications/{id}", efossNotification.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EfossNotification> efossNotificationList = notificationRepository.findAll();
        assertThat(efossNotificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
