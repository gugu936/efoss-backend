package com.lawrence.efoss.web.rest;

import com.lawrence.efoss.EfossApp;
import com.lawrence.efoss.domain.BillingAddress;
import com.lawrence.efoss.repository.BillingAddressRepository;
import com.lawrence.efoss.service.BillingAddressService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.lawrence.efoss.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BillingAddressResource} REST controller.
 */
@SpringBootTest(classes = EfossApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class BillingAddressResourceIT {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_STREET = "AAAAAAAAAA";
    private static final String UPDATED_STREET = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private BillingAddressRepository billingAddressRepository;

    @Autowired
    private BillingAddressService billingAddressService;

    @Autowired
    private MockMvc restBillingAddressMockMvc;

    private BillingAddress billingAddress;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BillingAddress createEntity() {
        BillingAddress billingAddress = new BillingAddress()
            .userId(DEFAULT_USER_ID)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .phone(DEFAULT_PHONE)
            .city(DEFAULT_CITY)
            .street(DEFAULT_STREET)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return billingAddress;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BillingAddress createUpdatedEntity() {
        BillingAddress billingAddress = new BillingAddress()
            .userId(UPDATED_USER_ID)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .city(UPDATED_CITY)
            .street(UPDATED_STREET)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return billingAddress;
    }

    @BeforeEach
    public void initTest() {
        billingAddressRepository.deleteAll();
        billingAddress = createEntity();
    }

    @Test
    public void createBillingAddress() throws Exception {
        int databaseSizeBeforeCreate = billingAddressRepository.findAll().size();

        // Create the BillingAddress
        restBillingAddressMockMvc.perform(post("/api/billing-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(billingAddress)))
            .andExpect(status().isCreated());

        // Validate the BillingAddress in the database
        List<BillingAddress> billingAddressList = billingAddressRepository.findAll();
        assertThat(billingAddressList).hasSize(databaseSizeBeforeCreate + 1);
        BillingAddress testBillingAddress = billingAddressList.get(billingAddressList.size() - 1);
        assertThat(testBillingAddress.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testBillingAddress.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testBillingAddress.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testBillingAddress.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testBillingAddress.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testBillingAddress.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testBillingAddress.getStreet()).isEqualTo(DEFAULT_STREET);
        assertThat(testBillingAddress.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testBillingAddress.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    public void createBillingAddressWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = billingAddressRepository.findAll().size();

        // Create the BillingAddress with an existing ID
        billingAddress.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restBillingAddressMockMvc.perform(post("/api/billing-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(billingAddress)))
            .andExpect(status().isBadRequest());

        // Validate the BillingAddress in the database
        List<BillingAddress> billingAddressList = billingAddressRepository.findAll();
        assertThat(billingAddressList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllBillingAddresses() throws Exception {
        // Initialize the database
        billingAddressRepository.save(billingAddress);

        // Get all the billingAddressList
        restBillingAddressMockMvc.perform(get("/api/billing-addresses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(billingAddress.getId())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].street").value(hasItem(DEFAULT_STREET)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))));
    }
    
    @Test
    public void getBillingAddress() throws Exception {
        // Initialize the database
        billingAddressRepository.save(billingAddress);

        // Get the billingAddress
        restBillingAddressMockMvc.perform(get("/api/billing-addresses/{id}", billingAddress.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(billingAddress.getId()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.street").value(DEFAULT_STREET))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    public void getNonExistingBillingAddress() throws Exception {
        // Get the billingAddress
        restBillingAddressMockMvc.perform(get("/api/billing-addresses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateBillingAddress() throws Exception {
        // Initialize the database
        billingAddressService.save(billingAddress);

        int databaseSizeBeforeUpdate = billingAddressRepository.findAll().size();

        // Update the billingAddress
        BillingAddress updatedBillingAddress = billingAddressRepository.findById(billingAddress.getId()).get();
        updatedBillingAddress
            .userId(UPDATED_USER_ID)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .phone(UPDATED_PHONE)
            .city(UPDATED_CITY)
            .street(UPDATED_STREET)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        restBillingAddressMockMvc.perform(put("/api/billing-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBillingAddress)))
            .andExpect(status().isOk());

        // Validate the BillingAddress in the database
        List<BillingAddress> billingAddressList = billingAddressRepository.findAll();
        assertThat(billingAddressList).hasSize(databaseSizeBeforeUpdate);
        BillingAddress testBillingAddress = billingAddressList.get(billingAddressList.size() - 1);
        assertThat(testBillingAddress.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testBillingAddress.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testBillingAddress.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testBillingAddress.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testBillingAddress.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testBillingAddress.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testBillingAddress.getStreet()).isEqualTo(UPDATED_STREET);
        assertThat(testBillingAddress.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testBillingAddress.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    public void updateNonExistingBillingAddress() throws Exception {
        int databaseSizeBeforeUpdate = billingAddressRepository.findAll().size();

        // Create the BillingAddress

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBillingAddressMockMvc.perform(put("/api/billing-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(billingAddress)))
            .andExpect(status().isBadRequest());

        // Validate the BillingAddress in the database
        List<BillingAddress> billingAddressList = billingAddressRepository.findAll();
        assertThat(billingAddressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteBillingAddress() throws Exception {
        // Initialize the database
        billingAddressService.save(billingAddress);

        int databaseSizeBeforeDelete = billingAddressRepository.findAll().size();

        // Delete the billingAddress
        restBillingAddressMockMvc.perform(delete("/api/billing-addresses/{id}", billingAddress.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BillingAddress> billingAddressList = billingAddressRepository.findAll();
        assertThat(billingAddressList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
