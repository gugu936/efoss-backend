package com.lawrence.efoss.web.rest;

import com.lawrence.efoss.EfossApp;
import com.lawrence.efoss.domain.ShippingMethod;
import com.lawrence.efoss.repository.ShippingMethodRepository;
import com.lawrence.efoss.service.ShippingMethodService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.lawrence.efoss.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ShippingMethodResource} REST controller.
 */
@SpringBootTest(classes = EfossApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ShippingMethodResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private ShippingMethodRepository shippingMethodRepository;

    @Autowired
    private ShippingMethodService shippingMethodService;

    @Autowired
    private MockMvc restShippingMethodMockMvc;

    private ShippingMethod shippingMethod;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShippingMethod createEntity() {
        ShippingMethod shippingMethod = new ShippingMethod()
            .name(DEFAULT_NAME)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT);
        return shippingMethod;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShippingMethod createUpdatedEntity() {
        ShippingMethod shippingMethod = new ShippingMethod()
            .name(UPDATED_NAME)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);
        return shippingMethod;
    }

    @BeforeEach
    public void initTest() {
        shippingMethodRepository.deleteAll();
        shippingMethod = createEntity();
    }

    @Test
    public void createShippingMethod() throws Exception {
        int databaseSizeBeforeCreate = shippingMethodRepository.findAll().size();

        // Create the ShippingMethod
        restShippingMethodMockMvc.perform(post("/api/shipping-methods")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shippingMethod)))
            .andExpect(status().isCreated());

        // Validate the ShippingMethod in the database
        List<ShippingMethod> shippingMethodList = shippingMethodRepository.findAll();
        assertThat(shippingMethodList).hasSize(databaseSizeBeforeCreate + 1);
        ShippingMethod testShippingMethod = shippingMethodList.get(shippingMethodList.size() - 1);
        assertThat(testShippingMethod.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testShippingMethod.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testShippingMethod.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    public void createShippingMethodWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shippingMethodRepository.findAll().size();

        // Create the ShippingMethod with an existing ID
        shippingMethod.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restShippingMethodMockMvc.perform(post("/api/shipping-methods")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shippingMethod)))
            .andExpect(status().isBadRequest());

        // Validate the ShippingMethod in the database
        List<ShippingMethod> shippingMethodList = shippingMethodRepository.findAll();
        assertThat(shippingMethodList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllShippingMethods() throws Exception {
        // Initialize the database
        shippingMethodRepository.save(shippingMethod);

        // Get all the shippingMethodList
        restShippingMethodMockMvc.perform(get("/api/shipping-methods?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shippingMethod.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))));
    }
    
    @Test
    public void getShippingMethod() throws Exception {
        // Initialize the database
        shippingMethodRepository.save(shippingMethod);

        // Get the shippingMethod
        restShippingMethodMockMvc.perform(get("/api/shipping-methods/{id}", shippingMethod.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(shippingMethod.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)));
    }

    @Test
    public void getNonExistingShippingMethod() throws Exception {
        // Get the shippingMethod
        restShippingMethodMockMvc.perform(get("/api/shipping-methods/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateShippingMethod() throws Exception {
        // Initialize the database
        shippingMethodService.save(shippingMethod);

        int databaseSizeBeforeUpdate = shippingMethodRepository.findAll().size();

        // Update the shippingMethod
        ShippingMethod updatedShippingMethod = shippingMethodRepository.findById(shippingMethod.getId()).get();
        updatedShippingMethod
            .name(UPDATED_NAME)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT);

        restShippingMethodMockMvc.perform(put("/api/shipping-methods")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedShippingMethod)))
            .andExpect(status().isOk());

        // Validate the ShippingMethod in the database
        List<ShippingMethod> shippingMethodList = shippingMethodRepository.findAll();
        assertThat(shippingMethodList).hasSize(databaseSizeBeforeUpdate);
        ShippingMethod testShippingMethod = shippingMethodList.get(shippingMethodList.size() - 1);
        assertThat(testShippingMethod.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testShippingMethod.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testShippingMethod.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    public void updateNonExistingShippingMethod() throws Exception {
        int databaseSizeBeforeUpdate = shippingMethodRepository.findAll().size();

        // Create the ShippingMethod

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShippingMethodMockMvc.perform(put("/api/shipping-methods")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(shippingMethod)))
            .andExpect(status().isBadRequest());

        // Validate the ShippingMethod in the database
        List<ShippingMethod> shippingMethodList = shippingMethodRepository.findAll();
        assertThat(shippingMethodList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteShippingMethod() throws Exception {
        // Initialize the database
        shippingMethodService.save(shippingMethod);

        int databaseSizeBeforeDelete = shippingMethodRepository.findAll().size();

        // Delete the shippingMethod
        restShippingMethodMockMvc.perform(delete("/api/shipping-methods/{id}", shippingMethod.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShippingMethod> shippingMethodList = shippingMethodRepository.findAll();
        assertThat(shippingMethodList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
