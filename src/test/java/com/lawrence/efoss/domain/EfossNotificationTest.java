package com.lawrence.efoss.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.lawrence.efoss.web.rest.TestUtil;

public class EfossNotificationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EfossNotification.class);
        EfossNotification efossNotification1 = new EfossNotification();
        efossNotification1.setId("id1");
        EfossNotification efossNotification2 = new EfossNotification();
        efossNotification2.setId(efossNotification1.getId());
        assertThat(efossNotification1).isEqualTo(efossNotification2);
        efossNotification2.setId("id2");
        assertThat(efossNotification1).isNotEqualTo(efossNotification2);
        efossNotification1.setId(null);
        assertThat(efossNotification1).isNotEqualTo(efossNotification2);
    }
}
