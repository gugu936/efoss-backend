package com.lawrence.efoss.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.lawrence.efoss.web.rest.TestUtil;

public class BillingAddressTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BillingAddress.class);
        BillingAddress billingAddress1 = new BillingAddress();
        billingAddress1.setId("id1");
        BillingAddress billingAddress2 = new BillingAddress();
        billingAddress2.setId(billingAddress1.getId());
        assertThat(billingAddress1).isEqualTo(billingAddress2);
        billingAddress2.setId("id2");
        assertThat(billingAddress1).isNotEqualTo(billingAddress2);
        billingAddress1.setId(null);
        assertThat(billingAddress1).isNotEqualTo(billingAddress2);
    }
}
