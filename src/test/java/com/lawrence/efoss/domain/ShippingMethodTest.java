package com.lawrence.efoss.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.lawrence.efoss.web.rest.TestUtil;

public class ShippingMethodTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShippingMethod.class);
        ShippingMethod shippingMethod1 = new ShippingMethod();
        shippingMethod1.setId("id1");
        ShippingMethod shippingMethod2 = new ShippingMethod();
        shippingMethod2.setId(shippingMethod1.getId());
        assertThat(shippingMethod1).isEqualTo(shippingMethod2);
        shippingMethod2.setId("id2");
        assertThat(shippingMethod1).isNotEqualTo(shippingMethod2);
        shippingMethod1.setId(null);
        assertThat(shippingMethod1).isNotEqualTo(shippingMethod2);
    }
}
