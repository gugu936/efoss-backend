import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EfossTestModule } from '../../../test.module';
import { ShippingMethodUpdateComponent } from 'app/entities/shipping-method/shipping-method-update.component';
import { ShippingMethodService } from 'app/entities/shipping-method/shipping-method.service';
import { ShippingMethod } from 'app/shared/model/shipping-method.model';

describe('Component Tests', () => {
  describe('ShippingMethod Management Update Component', () => {
    let comp: ShippingMethodUpdateComponent;
    let fixture: ComponentFixture<ShippingMethodUpdateComponent>;
    let service: ShippingMethodService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EfossTestModule],
        declarations: [ShippingMethodUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ShippingMethodUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShippingMethodUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShippingMethodService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShippingMethod('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ShippingMethod();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
