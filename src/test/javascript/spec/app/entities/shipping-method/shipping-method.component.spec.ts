import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EfossTestModule } from '../../../test.module';
import { ShippingMethodComponent } from 'app/entities/shipping-method/shipping-method.component';
import { ShippingMethodService } from 'app/entities/shipping-method/shipping-method.service';
import { ShippingMethod } from 'app/shared/model/shipping-method.model';

describe('Component Tests', () => {
  describe('ShippingMethod Management Component', () => {
    let comp: ShippingMethodComponent;
    let fixture: ComponentFixture<ShippingMethodComponent>;
    let service: ShippingMethodService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EfossTestModule],
        declarations: [ShippingMethodComponent]
      })
        .overrideTemplate(ShippingMethodComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ShippingMethodComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ShippingMethodService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ShippingMethod('123')],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.shippingMethods && comp.shippingMethods[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
