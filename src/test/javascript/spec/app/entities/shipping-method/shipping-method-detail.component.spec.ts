import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EfossTestModule } from '../../../test.module';
import { ShippingMethodDetailComponent } from 'app/entities/shipping-method/shipping-method-detail.component';
import { ShippingMethod } from 'app/shared/model/shipping-method.model';

describe('Component Tests', () => {
  describe('ShippingMethod Management Detail Component', () => {
    let comp: ShippingMethodDetailComponent;
    let fixture: ComponentFixture<ShippingMethodDetailComponent>;
    const route = ({ data: of({ shippingMethod: new ShippingMethod('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EfossTestModule],
        declarations: [ShippingMethodDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ShippingMethodDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ShippingMethodDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load shippingMethod on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.shippingMethod).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
