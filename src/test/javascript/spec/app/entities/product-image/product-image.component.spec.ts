import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EfossTestModule } from '../../../test.module';
import { ProductImageComponent } from 'app/entities/product-image/product-image.component';
import { ProductImageService } from 'app/entities/product-image/product-image.service';
import { ProductImage } from 'app/shared/model/product-image.model';

describe('Component Tests', () => {
  describe('ProductImage Management Component', () => {
    let comp: ProductImageComponent;
    let fixture: ComponentFixture<ProductImageComponent>;
    let service: ProductImageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EfossTestModule],
        declarations: [ProductImageComponent]
      })
        .overrideTemplate(ProductImageComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProductImageComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProductImageService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new ProductImage('123')],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.productImages && comp.productImages[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
