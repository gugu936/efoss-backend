import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EfossTestModule } from '../../../test.module';
import { BillingAddressDetailComponent } from 'app/entities/billing-address/billing-address-detail.component';
import { BillingAddress } from 'app/shared/model/billing-address.model';

describe('Component Tests', () => {
  describe('BillingAddress Management Detail Component', () => {
    let comp: BillingAddressDetailComponent;
    let fixture: ComponentFixture<BillingAddressDetailComponent>;
    const route = ({ data: of({ billingAddress: new BillingAddress('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EfossTestModule],
        declarations: [BillingAddressDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BillingAddressDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BillingAddressDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load billingAddress on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.billingAddress).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
