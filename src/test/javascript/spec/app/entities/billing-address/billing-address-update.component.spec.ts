import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EfossTestModule } from '../../../test.module';
import { BillingAddressUpdateComponent } from 'app/entities/billing-address/billing-address-update.component';
import { BillingAddressService } from 'app/entities/billing-address/billing-address.service';
import { BillingAddress } from 'app/shared/model/billing-address.model';

describe('Component Tests', () => {
  describe('BillingAddress Management Update Component', () => {
    let comp: BillingAddressUpdateComponent;
    let fixture: ComponentFixture<BillingAddressUpdateComponent>;
    let service: BillingAddressService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EfossTestModule],
        declarations: [BillingAddressUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BillingAddressUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BillingAddressUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BillingAddressService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BillingAddress('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BillingAddress();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
