import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EfossTestModule } from '../../../test.module';
import { BillingAddressComponent } from 'app/entities/billing-address/billing-address.component';
import { BillingAddressService } from 'app/entities/billing-address/billing-address.service';
import { BillingAddress } from 'app/shared/model/billing-address.model';

describe('Component Tests', () => {
  describe('BillingAddress Management Component', () => {
    let comp: BillingAddressComponent;
    let fixture: ComponentFixture<BillingAddressComponent>;
    let service: BillingAddressService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EfossTestModule],
        declarations: [BillingAddressComponent]
      })
        .overrideTemplate(BillingAddressComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BillingAddressComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BillingAddressService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BillingAddress('123')],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.billingAddresses && comp.billingAddresses[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
