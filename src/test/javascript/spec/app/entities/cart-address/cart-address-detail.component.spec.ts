import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EfossTestModule } from '../../../test.module';
import { CartAddressDetailComponent } from 'app/entities/cart-address/cart-address-detail.component';
import { CartAddress } from 'app/shared/model/cart-address.model';

describe('Component Tests', () => {
  describe('CartAddress Management Detail Component', () => {
    let comp: CartAddressDetailComponent;
    let fixture: ComponentFixture<CartAddressDetailComponent>;
    const route = ({ data: of({ cartAddress: new CartAddress('123') }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EfossTestModule],
        declarations: [CartAddressDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CartAddressDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CartAddressDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load cartAddress on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cartAddress).toEqual(jasmine.objectContaining({ id: '123' }));
      });
    });
  });
});
