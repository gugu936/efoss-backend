import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { EfossTestModule } from '../../../test.module';
import { CartAddressComponent } from 'app/entities/cart-address/cart-address.component';
import { CartAddressService } from 'app/entities/cart-address/cart-address.service';
import { CartAddress } from 'app/shared/model/cart-address.model';

describe('Component Tests', () => {
  describe('CartAddress Management Component', () => {
    let comp: CartAddressComponent;
    let fixture: ComponentFixture<CartAddressComponent>;
    let service: CartAddressService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EfossTestModule],
        declarations: [CartAddressComponent]
      })
        .overrideTemplate(CartAddressComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CartAddressComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CartAddressService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CartAddress('123')],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cartAddresses && comp.cartAddresses[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
  });
});
