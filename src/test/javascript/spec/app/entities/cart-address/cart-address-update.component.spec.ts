import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EfossTestModule } from '../../../test.module';
import { CartAddressUpdateComponent } from 'app/entities/cart-address/cart-address-update.component';
import { CartAddressService } from 'app/entities/cart-address/cart-address.service';
import { CartAddress } from 'app/shared/model/cart-address.model';

describe('Component Tests', () => {
  describe('CartAddress Management Update Component', () => {
    let comp: CartAddressUpdateComponent;
    let fixture: ComponentFixture<CartAddressUpdateComponent>;
    let service: CartAddressService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EfossTestModule],
        declarations: [CartAddressUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CartAddressUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CartAddressUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CartAddressService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CartAddress('123');
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CartAddress();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
