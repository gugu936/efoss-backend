import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { EfossSharedModule } from 'app/shared/shared.module';
import { EfossCoreModule } from 'app/core/core.module';
import { EfossAppRoutingModule } from './app-routing.module';
import { EfossHomeModule } from './home/home.module';
import { EfossEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';
import { AppHeaderComponent } from './layouts/full/header/header.component';
import { AppSidebarComponent } from './layouts/full/sidebar/sidebar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FullComponent } from './layouts/full/full.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SpinnerComponent } from './shared/spinner.component';
import { DemoMaterialModule } from './demo-material-module';
import { LeftMenuComponent } from './layouts/navbar/left-menu.component';
@NgModule({
  imports: [
    BrowserModule,
    EfossSharedModule,
    EfossCoreModule,
    EfossHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    EfossEntityModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    FlexLayoutModule,
    EfossAppRoutingModule
  ],
  declarations: [
    MainComponent,
    AppHeaderComponent,
    AppSidebarComponent,
    LeftMenuComponent,
    NavbarComponent,
    FullComponent,
    ErrorComponent,
    PageRibbonComponent,
    FooterComponent,
    SpinnerComponent
  ],
  bootstrap: [MainComponent]
})
export class EfossAppModule {}
