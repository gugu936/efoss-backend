import { Routes } from '@angular/router';

import { NavbarComponent } from './navbar.component';
import { LeftMenuComponent } from './left-menu.component';

export const navbarRoute: Routes = [
  {
    path: '',
    component: NavbarComponent,
    outlet: 'navbar'
  }
  // {
  //   path: '',
  //   component: LeftMenuComponent,
  //   children: [
  //     {
  //       path: '',
  //       redirectTo: '/dashboard',
  //       pathMatch: 'full'
  //     },
  //     {
  //       path: '',
  //       loadChildren: () => import('./../../material-component/material.module').then(m => m.MaterialComponentsModule)
  //     },
  //     {
  //       path: 'dashboard',
  //       loadChildren: () => import('./../../dashboard/dashboard.module').then(m => m.DashboardModule)
  //     }
  //   ],
  //   outlet: 'left-menu'
  // }
];
