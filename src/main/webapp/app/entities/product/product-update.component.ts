import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IProduct, Product } from 'app/shared/model/product.model';
import { ProductService } from './product.service';

@Component({
  selector: 'jhi-product-update',
  templateUrl: './product-update.component.html'
})
export class ProductUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    qty: [],
    description: [],
    companyId: [],
    brand: [],
    categoryId: [],
    price: [],
    refurbished: [],
    height: [],
    depth: [],
    width: [],
    shippingFee: [],
    weight: [],
    active: [],
    createdAt: [],
    updatedAt: []
  });

  constructor(protected productService: ProductService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ product }) => {
      if (!product.id) {
        const today = moment().startOf('day');
        product.createdAt = today;
        product.updatedAt = today;
      }

      this.updateForm(product);
    });
  }

  updateForm(product: IProduct): void {
    this.editForm.patchValue({
      id: product.id,
      name: product.name,
      qty: product.qty,
      description: product.description,
      companyId: product.companyId,
      brand: product.brand,
      categoryId: product.categoryId,
      price: product.price,
      refurbished: product.refurbished,
      height: product.height,
      depth: product.depth,
      width: product.width,
      shippingFee: product.shippingFee,
      weight: product.weight,
      active: product.active,
      createdAt: product.createdAt ? product.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: product.updatedAt ? product.updatedAt.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const product = this.createFromForm();
    if (product.id !== undefined) {
      this.subscribeToSaveResponse(this.productService.update(product));
    } else {
      this.subscribeToSaveResponse(this.productService.create(product));
    }
  }

  private createFromForm(): IProduct {
    return {
      ...new Product(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      qty: this.editForm.get(['qty'])!.value,
      description: this.editForm.get(['description'])!.value,
      companyId: this.editForm.get(['companyId'])!.value,
      brand: this.editForm.get(['brand'])!.value,
      categoryId: this.editForm.get(['categoryId'])!.value,
      price: this.editForm.get(['price'])!.value,
      refurbished: this.editForm.get(['refurbished'])!.value,
      height: this.editForm.get(['height'])!.value,
      depth: this.editForm.get(['depth'])!.value,
      width: this.editForm.get(['width'])!.value,
      shippingFee: this.editForm.get(['shippingFee'])!.value,
      weight: this.editForm.get(['weight'])!.value,
      active: this.editForm.get(['active'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduct>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
