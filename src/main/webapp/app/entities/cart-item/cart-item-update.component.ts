import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICartItem, CartItem } from 'app/shared/model/cart-item.model';
import { CartItemService } from './cart-item.service';

@Component({
  selector: 'jhi-cart-item-update',
  templateUrl: './cart-item-update.component.html'
})
export class CartItemUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    cartId: [],
    productId: [],
    color: [],
    size: [],
    qty: [],
    price: [],
    createdAt: [],
    updatedAt: []
  });

  constructor(protected cartItemService: CartItemService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cartItem }) => {
      if (!cartItem.id) {
        const today = moment().startOf('day');
        cartItem.createdAt = today;
        cartItem.updatedAt = today;
      }

      this.updateForm(cartItem);
    });
  }

  updateForm(cartItem: ICartItem): void {
    this.editForm.patchValue({
      id: cartItem.id,
      cartId: cartItem.cartId,
      productId: cartItem.productId,
      color: cartItem.color,
      size: cartItem.size,
      qty: cartItem.qty,
      price: cartItem.price,
      createdAt: cartItem.createdAt ? cartItem.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: cartItem.updatedAt ? cartItem.updatedAt.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cartItem = this.createFromForm();
    if (cartItem.id !== undefined) {
      this.subscribeToSaveResponse(this.cartItemService.update(cartItem));
    } else {
      this.subscribeToSaveResponse(this.cartItemService.create(cartItem));
    }
  }

  private createFromForm(): ICartItem {
    return {
      ...new CartItem(),
      id: this.editForm.get(['id'])!.value,
      cartId: this.editForm.get(['cartId'])!.value,
      productId: this.editForm.get(['productId'])!.value,
      color: this.editForm.get(['color'])!.value,
      size: this.editForm.get(['size'])!.value,
      qty: this.editForm.get(['qty'])!.value,
      price: this.editForm.get(['price'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICartItem>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
