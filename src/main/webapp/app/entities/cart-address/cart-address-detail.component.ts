import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICartAddress } from 'app/shared/model/cart-address.model';

@Component({
  selector: 'jhi-cart-address-detail',
  templateUrl: './cart-address-detail.component.html'
})
export class CartAddressDetailComponent implements OnInit {
  cartAddress: ICartAddress | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cartAddress }) => (this.cartAddress = cartAddress));
  }

  previousState(): void {
    window.history.back();
  }
}
