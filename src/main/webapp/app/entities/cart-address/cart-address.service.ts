import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICartAddress } from 'app/shared/model/cart-address.model';

type EntityResponseType = HttpResponse<ICartAddress>;
type EntityArrayResponseType = HttpResponse<ICartAddress[]>;

@Injectable({ providedIn: 'root' })
export class CartAddressService {
  public resourceUrl = SERVER_API_URL + 'api/cart-addresses';

  constructor(protected http: HttpClient) {}

  create(cartAddress: ICartAddress): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cartAddress);
    return this.http
      .post<ICartAddress>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(cartAddress: ICartAddress): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cartAddress);
    return this.http
      .put<ICartAddress>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<ICartAddress>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICartAddress[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(cartAddress: ICartAddress): ICartAddress {
    const copy: ICartAddress = Object.assign({}, cartAddress, {
      createdAt: cartAddress.createdAt && cartAddress.createdAt.isValid() ? cartAddress.createdAt.toJSON() : undefined,
      updatedAt: cartAddress.updatedAt && cartAddress.updatedAt.isValid() ? cartAddress.updatedAt.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((cartAddress: ICartAddress) => {
        cartAddress.createdAt = cartAddress.createdAt ? moment(cartAddress.createdAt) : undefined;
        cartAddress.updatedAt = cartAddress.updatedAt ? moment(cartAddress.updatedAt) : undefined;
      });
    }
    return res;
  }
}
