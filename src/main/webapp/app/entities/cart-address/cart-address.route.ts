import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICartAddress, CartAddress } from 'app/shared/model/cart-address.model';
import { CartAddressService } from './cart-address.service';
import { CartAddressComponent } from './cart-address.component';
import { CartAddressDetailComponent } from './cart-address-detail.component';
import { CartAddressUpdateComponent } from './cart-address-update.component';

@Injectable({ providedIn: 'root' })
export class CartAddressResolve implements Resolve<ICartAddress> {
  constructor(private service: CartAddressService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICartAddress> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((cartAddress: HttpResponse<CartAddress>) => {
          if (cartAddress.body) {
            return of(cartAddress.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CartAddress());
  }
}

export const cartAddressRoute: Routes = [
  {
    path: '',
    component: CartAddressComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CartAddresses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CartAddressDetailComponent,
    resolve: {
      cartAddress: CartAddressResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CartAddresses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CartAddressUpdateComponent,
    resolve: {
      cartAddress: CartAddressResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CartAddresses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CartAddressUpdateComponent,
    resolve: {
      cartAddress: CartAddressResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CartAddresses'
    },
    canActivate: [UserRouteAccessService]
  }
];
