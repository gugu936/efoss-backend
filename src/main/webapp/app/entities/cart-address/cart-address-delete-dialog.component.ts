import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICartAddress } from 'app/shared/model/cart-address.model';
import { CartAddressService } from './cart-address.service';

@Component({
  templateUrl: './cart-address-delete-dialog.component.html'
})
export class CartAddressDeleteDialogComponent {
  cartAddress?: ICartAddress;

  constructor(
    protected cartAddressService: CartAddressService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.cartAddressService.delete(id).subscribe(() => {
      this.eventManager.broadcast('cartAddressListModification');
      this.activeModal.close();
    });
  }
}
