import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICartAddress, CartAddress } from 'app/shared/model/cart-address.model';
import { CartAddressService } from './cart-address.service';

@Component({
  selector: 'jhi-cart-address-update',
  templateUrl: './cart-address-update.component.html'
})
export class CartAddressUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    cartId: [],
    firstName: [],
    lastName: [],
    phone: [],
    city: [],
    street: [],
    createdAt: [],
    updatedAt: []
  });

  constructor(protected cartAddressService: CartAddressService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cartAddress }) => {
      if (!cartAddress.id) {
        const today = moment().startOf('day');
        cartAddress.createdAt = today;
        cartAddress.updatedAt = today;
      }

      this.updateForm(cartAddress);
    });
  }

  updateForm(cartAddress: ICartAddress): void {
    this.editForm.patchValue({
      id: cartAddress.id,
      cartId: cartAddress.cartId,
      firstName: cartAddress.firstName,
      lastName: cartAddress.lastName,
      phone: cartAddress.phone,
      city: cartAddress.city,
      street: cartAddress.street,
      createdAt: cartAddress.createdAt ? cartAddress.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: cartAddress.updatedAt ? cartAddress.updatedAt.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cartAddress = this.createFromForm();
    if (cartAddress.id !== undefined) {
      this.subscribeToSaveResponse(this.cartAddressService.update(cartAddress));
    } else {
      this.subscribeToSaveResponse(this.cartAddressService.create(cartAddress));
    }
  }

  private createFromForm(): ICartAddress {
    return {
      ...new CartAddress(),
      id: this.editForm.get(['id'])!.value,
      cartId: this.editForm.get(['cartId'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      phone: this.editForm.get(['phone'])!.value,
      city: this.editForm.get(['city'])!.value,
      street: this.editForm.get(['street'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICartAddress>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
