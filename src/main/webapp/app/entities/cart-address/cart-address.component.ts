import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICartAddress } from 'app/shared/model/cart-address.model';
import { CartAddressService } from './cart-address.service';
import { CartAddressDeleteDialogComponent } from './cart-address-delete-dialog.component';

@Component({
  selector: 'jhi-cart-address',
  templateUrl: './cart-address.component.html'
})
export class CartAddressComponent implements OnInit, OnDestroy {
  cartAddresses?: ICartAddress[];
  eventSubscriber?: Subscription;

  constructor(
    protected cartAddressService: CartAddressService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.cartAddressService.query().subscribe((res: HttpResponse<ICartAddress[]>) => (this.cartAddresses = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCartAddresses();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICartAddress): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCartAddresses(): void {
    this.eventSubscriber = this.eventManager.subscribe('cartAddressListModification', () => this.loadAll());
  }

  delete(cartAddress: ICartAddress): void {
    const modalRef = this.modalService.open(CartAddressDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.cartAddress = cartAddress;
  }
}
