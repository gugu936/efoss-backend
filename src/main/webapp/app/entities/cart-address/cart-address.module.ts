import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EfossSharedModule } from 'app/shared/shared.module';
import { CartAddressComponent } from './cart-address.component';
import { CartAddressDetailComponent } from './cart-address-detail.component';
import { CartAddressUpdateComponent } from './cart-address-update.component';
import { CartAddressDeleteDialogComponent } from './cart-address-delete-dialog.component';
import { cartAddressRoute } from './cart-address.route';

@NgModule({
  imports: [EfossSharedModule, RouterModule.forChild(cartAddressRoute)],
  declarations: [CartAddressComponent, CartAddressDetailComponent, CartAddressUpdateComponent, CartAddressDeleteDialogComponent],
  entryComponents: [CartAddressDeleteDialogComponent]
})
export class EfossCartAddressModule {}
