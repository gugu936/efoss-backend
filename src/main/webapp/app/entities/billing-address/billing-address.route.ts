import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBillingAddress, BillingAddress } from 'app/shared/model/billing-address.model';
import { BillingAddressService } from './billing-address.service';
import { BillingAddressComponent } from './billing-address.component';
import { BillingAddressDetailComponent } from './billing-address-detail.component';
import { BillingAddressUpdateComponent } from './billing-address-update.component';

@Injectable({ providedIn: 'root' })
export class BillingAddressResolve implements Resolve<IBillingAddress> {
  constructor(private service: BillingAddressService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBillingAddress> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((billingAddress: HttpResponse<BillingAddress>) => {
          if (billingAddress.body) {
            return of(billingAddress.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BillingAddress());
  }
}

export const billingAddressRoute: Routes = [
  {
    path: '',
    component: BillingAddressComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BillingAddresses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BillingAddressDetailComponent,
    resolve: {
      billingAddress: BillingAddressResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BillingAddresses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BillingAddressUpdateComponent,
    resolve: {
      billingAddress: BillingAddressResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BillingAddresses'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BillingAddressUpdateComponent,
    resolve: {
      billingAddress: BillingAddressResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'BillingAddresses'
    },
    canActivate: [UserRouteAccessService]
  }
];
