import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBillingAddress } from 'app/shared/model/billing-address.model';
import { BillingAddressService } from './billing-address.service';

@Component({
  templateUrl: './billing-address-delete-dialog.component.html'
})
export class BillingAddressDeleteDialogComponent {
  billingAddress?: IBillingAddress;

  constructor(
    protected billingAddressService: BillingAddressService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.billingAddressService.delete(id).subscribe(() => {
      this.eventManager.broadcast('billingAddressListModification');
      this.activeModal.close();
    });
  }
}
