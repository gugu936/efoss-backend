import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductImage } from 'app/shared/model/product-image.model';
import { ProductImageService } from './product-image.service';
import { ProductImageDeleteDialogComponent } from './product-image-delete-dialog.component';

@Component({
  selector: 'jhi-product-image',
  templateUrl: './product-image.component.html'
})
export class ProductImageComponent implements OnInit, OnDestroy {
  productImages?: IProductImage[];
  eventSubscriber?: Subscription;

  constructor(
    protected productImageService: ProductImageService,
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.productImageService.query().subscribe((res: HttpResponse<IProductImage[]>) => (this.productImages = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProductImages();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductImage): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInProductImages(): void {
    this.eventSubscriber = this.eventManager.subscribe('productImageListModification', () => this.loadAll());
  }

  delete(productImage: IProductImage): void {
    const modalRef = this.modalService.open(ProductImageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productImage = productImage;
  }
}
