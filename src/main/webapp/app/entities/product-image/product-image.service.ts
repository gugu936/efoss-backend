import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProductImage } from 'app/shared/model/product-image.model';

type EntityResponseType = HttpResponse<IProductImage>;
type EntityArrayResponseType = HttpResponse<IProductImage[]>;

@Injectable({ providedIn: 'root' })
export class ProductImageService {
  public resourceUrl = SERVER_API_URL + 'api/product-images';

  constructor(protected http: HttpClient) {}

  create(productImage: IProductImage): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(productImage);
    return this.http
      .post<IProductImage>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(productImage: IProductImage): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(productImage);
    return this.http
      .put<IProductImage>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IProductImage>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IProductImage[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(productImage: IProductImage): IProductImage {
    const copy: IProductImage = Object.assign({}, productImage, {
      createdAt: productImage.createdAt && productImage.createdAt.isValid() ? productImage.createdAt.toJSON() : undefined,
      updatedAt: productImage.updatedAt && productImage.updatedAt.isValid() ? productImage.updatedAt.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((productImage: IProductImage) => {
        productImage.createdAt = productImage.createdAt ? moment(productImage.createdAt) : undefined;
        productImage.updatedAt = productImage.updatedAt ? moment(productImage.updatedAt) : undefined;
      });
    }
    return res;
  }
}
