import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IWishlist, Wishlist } from 'app/shared/model/wishlist.model';
import { WishlistService } from './wishlist.service';

@Component({
  selector: 'jhi-wishlist-update',
  templateUrl: './wishlist-update.component.html'
})
export class WishlistUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    userId: [],
    productIds: [],
    createdAt: [],
    updatedAt: []
  });

  constructor(protected wishlistService: WishlistService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ wishlist }) => {
      if (!wishlist.id) {
        const today = moment().startOf('day');
        wishlist.createdAt = today;
        wishlist.updatedAt = today;
      }

      this.updateForm(wishlist);
    });
  }

  updateForm(wishlist: IWishlist): void {
    this.editForm.patchValue({
      id: wishlist.id,
      userId: wishlist.userId,
      productIds: wishlist.productIds,
      createdAt: wishlist.createdAt ? wishlist.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: wishlist.updatedAt ? wishlist.updatedAt.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const wishlist = this.createFromForm();
    if (wishlist.id !== undefined) {
      this.subscribeToSaveResponse(this.wishlistService.update(wishlist));
    } else {
      this.subscribeToSaveResponse(this.wishlistService.create(wishlist));
    }
  }

  private createFromForm(): IWishlist {
    return {
      ...new Wishlist(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      productIds: this.editForm.get(['productIds'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IWishlist>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
