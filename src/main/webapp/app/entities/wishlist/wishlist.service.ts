import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IWishlist } from 'app/shared/model/wishlist.model';

type EntityResponseType = HttpResponse<IWishlist>;
type EntityArrayResponseType = HttpResponse<IWishlist[]>;

@Injectable({ providedIn: 'root' })
export class WishlistService {
  public resourceUrl = SERVER_API_URL + 'api/wishlists';

  constructor(protected http: HttpClient) {}

  create(wishlist: IWishlist): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(wishlist);
    return this.http
      .post<IWishlist>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(wishlist: IWishlist): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(wishlist);
    return this.http
      .put<IWishlist>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IWishlist>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IWishlist[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(wishlist: IWishlist): IWishlist {
    const copy: IWishlist = Object.assign({}, wishlist, {
      createdAt: wishlist.createdAt && wishlist.createdAt.isValid() ? wishlist.createdAt.toJSON() : undefined,
      updatedAt: wishlist.updatedAt && wishlist.updatedAt.isValid() ? wishlist.updatedAt.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((wishlist: IWishlist) => {
        wishlist.createdAt = wishlist.createdAt ? moment(wishlist.createdAt) : undefined;
        wishlist.updatedAt = wishlist.updatedAt ? moment(wishlist.updatedAt) : undefined;
      });
    }
    return res;
  }
}
