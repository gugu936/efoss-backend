import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EfossSharedModule } from 'app/shared/shared.module';
import { WishlistComponent } from './wishlist.component';
import { WishlistDetailComponent } from './wishlist-detail.component';
import { WishlistUpdateComponent } from './wishlist-update.component';
import { WishlistDeleteDialogComponent } from './wishlist-delete-dialog.component';
import { wishlistRoute } from './wishlist.route';

@NgModule({
  imports: [EfossSharedModule, RouterModule.forChild(wishlistRoute)],
  declarations: [WishlistComponent, WishlistDetailComponent, WishlistUpdateComponent, WishlistDeleteDialogComponent],
  entryComponents: [WishlistDeleteDialogComponent]
})
export class EfossWishlistModule {}
