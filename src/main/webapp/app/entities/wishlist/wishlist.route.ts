import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IWishlist, Wishlist } from 'app/shared/model/wishlist.model';
import { WishlistService } from './wishlist.service';
import { WishlistComponent } from './wishlist.component';
import { WishlistDetailComponent } from './wishlist-detail.component';
import { WishlistUpdateComponent } from './wishlist-update.component';

@Injectable({ providedIn: 'root' })
export class WishlistResolve implements Resolve<IWishlist> {
  constructor(private service: WishlistService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IWishlist> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((wishlist: HttpResponse<Wishlist>) => {
          if (wishlist.body) {
            return of(wishlist.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Wishlist());
  }
}

export const wishlistRoute: Routes = [
  {
    path: '',
    component: WishlistComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Wishlists'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: WishlistDetailComponent,
    resolve: {
      wishlist: WishlistResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Wishlists'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: WishlistUpdateComponent,
    resolve: {
      wishlist: WishlistResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Wishlists'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: WishlistUpdateComponent,
    resolve: {
      wishlist: WishlistResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Wishlists'
    },
    canActivate: [UserRouteAccessService]
  }
];
