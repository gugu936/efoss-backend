import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IWishlist } from 'app/shared/model/wishlist.model';
import { WishlistService } from './wishlist.service';
import { WishlistDeleteDialogComponent } from './wishlist-delete-dialog.component';

@Component({
  selector: 'jhi-wishlist',
  templateUrl: './wishlist.component.html'
})
export class WishlistComponent implements OnInit, OnDestroy {
  wishlists?: IWishlist[];
  eventSubscriber?: Subscription;

  constructor(protected wishlistService: WishlistService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.wishlistService.query().subscribe((res: HttpResponse<IWishlist[]>) => (this.wishlists = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInWishlists();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IWishlist): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInWishlists(): void {
    this.eventSubscriber = this.eventManager.subscribe('wishlistListModification', () => this.loadAll());
  }

  delete(wishlist: IWishlist): void {
    const modalRef = this.modalService.open(WishlistDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.wishlist = wishlist;
  }
}
