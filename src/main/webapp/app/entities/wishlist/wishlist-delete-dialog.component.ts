import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IWishlist } from 'app/shared/model/wishlist.model';
import { WishlistService } from './wishlist.service';

@Component({
  templateUrl: './wishlist-delete-dialog.component.html'
})
export class WishlistDeleteDialogComponent {
  wishlist?: IWishlist;

  constructor(protected wishlistService: WishlistService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.wishlistService.delete(id).subscribe(() => {
      this.eventManager.broadcast('wishlistListModification');
      this.activeModal.close();
    });
  }
}
