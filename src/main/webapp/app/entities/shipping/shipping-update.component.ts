import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IShipping, Shipping } from 'app/shared/model/shipping.model';
import { ShippingService } from './shipping.service';

@Component({
  selector: 'jhi-shipping-update',
  templateUrl: './shipping-update.component.html'
})
export class ShippingUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    orderId: [],
    trackingCode: [],
    carrier: [],
    weight: [],
    fee: [],
    createdAt: [],
    updatedAt: []
  });

  constructor(protected shippingService: ShippingService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shipping }) => {
      if (!shipping.id) {
        const today = moment().startOf('day');
        shipping.createdAt = today;
        shipping.updatedAt = today;
      }

      this.updateForm(shipping);
    });
  }

  updateForm(shipping: IShipping): void {
    this.editForm.patchValue({
      id: shipping.id,
      orderId: shipping.orderId,
      trackingCode: shipping.trackingCode,
      carrier: shipping.carrier,
      weight: shipping.weight,
      fee: shipping.fee,
      createdAt: shipping.createdAt ? shipping.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: shipping.updatedAt ? shipping.updatedAt.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shipping = this.createFromForm();
    if (shipping.id !== undefined) {
      this.subscribeToSaveResponse(this.shippingService.update(shipping));
    } else {
      this.subscribeToSaveResponse(this.shippingService.create(shipping));
    }
  }

  private createFromForm(): IShipping {
    return {
      ...new Shipping(),
      id: this.editForm.get(['id'])!.value,
      orderId: this.editForm.get(['orderId'])!.value,
      trackingCode: this.editForm.get(['trackingCode'])!.value,
      carrier: this.editForm.get(['carrier'])!.value,
      weight: this.editForm.get(['weight'])!.value,
      fee: this.editForm.get(['fee'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShipping>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
