import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IShippingMethod } from 'app/shared/model/shipping-method.model';
import { ShippingMethodService } from './shipping-method.service';

@Component({
  templateUrl: './shipping-method-delete-dialog.component.html'
})
export class ShippingMethodDeleteDialogComponent {
  shippingMethod?: IShippingMethod;

  constructor(
    protected shippingMethodService: ShippingMethodService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: string): void {
    this.shippingMethodService.delete(id).subscribe(() => {
      this.eventManager.broadcast('shippingMethodListModification');
      this.activeModal.close();
    });
  }
}
