import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IShippingMethod, ShippingMethod } from 'app/shared/model/shipping-method.model';
import { ShippingMethodService } from './shipping-method.service';

@Component({
  selector: 'jhi-shipping-method-update',
  templateUrl: './shipping-method-update.component.html'
})
export class ShippingMethodUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
    createdAt: [],
    updatedAt: []
  });

  constructor(protected shippingMethodService: ShippingMethodService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shippingMethod }) => {
      if (!shippingMethod.id) {
        const today = moment().startOf('day');
        shippingMethod.createdAt = today;
        shippingMethod.updatedAt = today;
      }

      this.updateForm(shippingMethod);
    });
  }

  updateForm(shippingMethod: IShippingMethod): void {
    this.editForm.patchValue({
      id: shippingMethod.id,
      name: shippingMethod.name,
      createdAt: shippingMethod.createdAt ? shippingMethod.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: shippingMethod.updatedAt ? shippingMethod.updatedAt.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const shippingMethod = this.createFromForm();
    if (shippingMethod.id !== undefined) {
      this.subscribeToSaveResponse(this.shippingMethodService.update(shippingMethod));
    } else {
      this.subscribeToSaveResponse(this.shippingMethodService.create(shippingMethod));
    }
  }

  private createFromForm(): IShippingMethod {
    return {
      ...new ShippingMethod(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IShippingMethod>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
