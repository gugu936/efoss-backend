import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IShippingMethod, ShippingMethod } from 'app/shared/model/shipping-method.model';
import { ShippingMethodService } from './shipping-method.service';
import { ShippingMethodComponent } from './shipping-method.component';
import { ShippingMethodDetailComponent } from './shipping-method-detail.component';
import { ShippingMethodUpdateComponent } from './shipping-method-update.component';

@Injectable({ providedIn: 'root' })
export class ShippingMethodResolve implements Resolve<IShippingMethod> {
  constructor(private service: ShippingMethodService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IShippingMethod> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((shippingMethod: HttpResponse<ShippingMethod>) => {
          if (shippingMethod.body) {
            return of(shippingMethod.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ShippingMethod());
  }
}

export const shippingMethodRoute: Routes = [
  {
    path: '',
    component: ShippingMethodComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ShippingMethods'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ShippingMethodDetailComponent,
    resolve: {
      shippingMethod: ShippingMethodResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ShippingMethods'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ShippingMethodUpdateComponent,
    resolve: {
      shippingMethod: ShippingMethodResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ShippingMethods'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ShippingMethodUpdateComponent,
    resolve: {
      shippingMethod: ShippingMethodResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ShippingMethods'
    },
    canActivate: [UserRouteAccessService]
  }
];
