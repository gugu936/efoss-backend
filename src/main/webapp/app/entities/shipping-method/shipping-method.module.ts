import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EfossSharedModule } from 'app/shared/shared.module';
import { ShippingMethodComponent } from './shipping-method.component';
import { ShippingMethodDetailComponent } from './shipping-method-detail.component';
import { ShippingMethodUpdateComponent } from './shipping-method-update.component';
import { ShippingMethodDeleteDialogComponent } from './shipping-method-delete-dialog.component';
import { shippingMethodRoute } from './shipping-method.route';

@NgModule({
  imports: [EfossSharedModule, RouterModule.forChild(shippingMethodRoute)],
  declarations: [
    ShippingMethodComponent,
    ShippingMethodDetailComponent,
    ShippingMethodUpdateComponent,
    ShippingMethodDeleteDialogComponent
  ],
  entryComponents: [ShippingMethodDeleteDialogComponent]
})
export class EfossShippingMethodModule {}
