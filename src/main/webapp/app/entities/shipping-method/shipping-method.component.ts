import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IShippingMethod } from 'app/shared/model/shipping-method.model';
import { ShippingMethodService } from './shipping-method.service';
import { ShippingMethodDeleteDialogComponent } from './shipping-method-delete-dialog.component';

@Component({
  selector: 'jhi-shipping-method',
  templateUrl: './shipping-method.component.html'
})
export class ShippingMethodComponent implements OnInit, OnDestroy {
  shippingMethods?: IShippingMethod[];
  eventSubscriber?: Subscription;

  constructor(
    protected shippingMethodService: ShippingMethodService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.shippingMethodService.query().subscribe((res: HttpResponse<IShippingMethod[]>) => (this.shippingMethods = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInShippingMethods();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IShippingMethod): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInShippingMethods(): void {
    this.eventSubscriber = this.eventManager.subscribe('shippingMethodListModification', () => this.loadAll());
  }

  delete(shippingMethod: IShippingMethod): void {
    const modalRef = this.modalService.open(ShippingMethodDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.shippingMethod = shippingMethod;
  }
}
