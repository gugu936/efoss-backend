import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IShippingMethod } from 'app/shared/model/shipping-method.model';

@Component({
  selector: 'jhi-shipping-method-detail',
  templateUrl: './shipping-method-detail.component.html'
})
export class ShippingMethodDetailComponent implements OnInit {
  shippingMethod: IShippingMethod | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ shippingMethod }) => (this.shippingMethod = shippingMethod));
  }

  previousState(): void {
    window.history.back();
  }
}
