import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICoin, Coin } from 'app/shared/model/coin.model';
import { CoinService } from './coin.service';

@Component({
  selector: 'jhi-coin-update',
  templateUrl: './coin-update.component.html'
})
export class CoinUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    userId: [],
    change: [],
    amount: [],
    status: [],
    createdAt: [],
    updatedAt: []
  });

  constructor(protected coinService: CoinService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coin }) => {
      if (!coin.id) {
        const today = moment().startOf('day');
        coin.createdAt = today;
        coin.updatedAt = today;
      }

      this.updateForm(coin);
    });
  }

  updateForm(coin: ICoin): void {
    this.editForm.patchValue({
      id: coin.id,
      userId: coin.userId,
      change: coin.change,
      amount: coin.amount,
      status: coin.status,
      createdAt: coin.createdAt ? coin.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: coin.updatedAt ? coin.updatedAt.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const coin = this.createFromForm();
    if (coin.id !== undefined) {
      this.subscribeToSaveResponse(this.coinService.update(coin));
    } else {
      this.subscribeToSaveResponse(this.coinService.create(coin));
    }
  }

  private createFromForm(): ICoin {
    return {
      ...new Coin(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      change: this.editForm.get(['change'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      status: this.editForm.get(['status'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoin>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
