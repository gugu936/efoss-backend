import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.EfossProductModule)
      },
      {
        path: 'rating',
        loadChildren: () => import('./rating/rating.module').then(m => m.EfossRatingModule)
      },
      {
        path: 'comment',
        loadChildren: () => import('./comment/comment.module').then(m => m.EfossCommentModule)
      },
      {
        path: 'address',
        loadChildren: () => import('./address/address.module').then(m => m.EfossAddressModule)
      },
      {
        path: 'order',
        loadChildren: () => import('./order/order.module').then(m => m.EfossOrderModule)
      },
      {
        path: 'invoice',
        loadChildren: () => import('./invoice/invoice.module').then(m => m.EfossInvoiceModule)
      },
      {
        path: 'product-image',
        loadChildren: () => import('./product-image/product-image.module').then(m => m.EfossProductImageModule)
      },
      {
        path: 'cart',
        loadChildren: () => import('./cart/cart.module').then(m => m.EfossCartModule)
      },
      {
        path: 'cart-item',
        loadChildren: () => import('./cart-item/cart-item.module').then(m => m.EfossCartItemModule)
      },
      {
        path: 'company',
        loadChildren: () => import('./company/company.module').then(m => m.EfossCompanyModule)
      },
      {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.EfossCategoryModule)
      },
      {
        path: 'cart-address',
        loadChildren: () => import('./cart-address/cart-address.module').then(m => m.EfossCartAddressModule)
      },
      {
        path: 'payment-method',
        loadChildren: () => import('./payment-method/payment-method.module').then(m => m.EfossPaymentMethodModule)
      },
      {
        path: 'delivery-address',
        loadChildren: () => import('./delivery-address/delivery-address.module').then(m => m.EfossDeliveryAddressModule)
      },
      {
        path: 'billing-address',
        loadChildren: () => import('./billing-address/billing-address.module').then(m => m.EfossBillingAddressModule)
      },
      {
        path: 'customer-order',
        loadChildren: () => import('./customer-order/customer-order.module').then(m => m.EfossCustomerOrderModule)
      },
      {
        path: 'shipping',
        loadChildren: () => import('./shipping/shipping.module').then(m => m.EfossShippingModule)
      },
      {
        path: 'shipping-method',
        loadChildren: () => import('./shipping-method/shipping-method.module').then(m => m.EfossShippingMethodModule)
      },
      {
        path: 'wishlist',
        loadChildren: () => import('./wishlist/wishlist.module').then(m => m.EfossWishlistModule)
      },
      {
        path: 'coin',
        loadChildren: () => import('./coin/coin.module').then(m => m.EfossCoinModule)
      },
      {
        path: 'notification',
        loadChildren: () => import('./notification/notification.module').then(m => m.EfossNotificationModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class EfossEntityModule {}
