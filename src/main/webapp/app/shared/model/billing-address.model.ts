import { Moment } from 'moment';

export interface IBillingAddress {
  id?: string;
  userId?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  city?: string;
  street?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class BillingAddress implements IBillingAddress {
  constructor(
    public id?: string,
    public userId?: string,
    public firstName?: string,
    public lastName?: string,
    public email?: string,
    public phone?: string,
    public city?: string,
    public street?: string,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
