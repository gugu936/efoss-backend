import { Moment } from 'moment';

export interface IWishlist {
  id?: string;
  userId?: string;
  productIds?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Wishlist implements IWishlist {
  constructor(
    public id?: string,
    public userId?: string,
    public productIds?: string,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
