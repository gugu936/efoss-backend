import { Moment } from 'moment';

export interface INotification {
  id?: string;
  userId?: string;
  message?: string;
  type?: string;
  status?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Notification implements INotification {
  constructor(
    public id?: string,
    public userId?: string,
    public message?: string,
    public type?: string,
    public status?: string,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
