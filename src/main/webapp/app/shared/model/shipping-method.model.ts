import { Moment } from 'moment';

export interface IShippingMethod {
  id?: string;
  name?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class ShippingMethod implements IShippingMethod {
  constructor(public id?: string, public name?: string, public createdAt?: Moment, public updatedAt?: Moment) {}
}
