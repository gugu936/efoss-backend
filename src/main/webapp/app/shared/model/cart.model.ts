import { Moment } from 'moment';

export interface ICart {
  id?: string;
  userId?: string;
  status?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Cart implements ICart {
  constructor(public id?: string, public userId?: string, public status?: string, public createdAt?: Moment, public updatedAt?: Moment) {}
}
