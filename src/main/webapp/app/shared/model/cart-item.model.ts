import { Moment } from 'moment';

export interface ICartItem {
  id?: string;
  cartId?: string;
  productId?: string;
  color?: string;
  size?: string;
  qty?: number;
  price?: number;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class CartItem implements ICartItem {
  constructor(
    public id?: string,
    public cartId?: string,
    public productId?: string,
    public color?: string,
    public size?: string,
    public qty?: number,
    public price?: number,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
