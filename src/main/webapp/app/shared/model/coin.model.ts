import { Moment } from 'moment';

export interface ICoin {
  id?: string;
  userId?: string;
  change?: string;
  amount?: number;
  status?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Coin implements ICoin {
  constructor(
    public id?: string,
    public userId?: string,
    public change?: string,
    public amount?: number,
    public status?: string,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
