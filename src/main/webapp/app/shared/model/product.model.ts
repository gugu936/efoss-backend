import { Moment } from 'moment';

export interface IProduct {
  id?: string;
  name?: string;
  qty?: number;
  description?: string;
  companyId?: string;
  brand?: string;
  categoryId?: string;
  price?: number;
  refurbished?: boolean;
  height?: number;
  depth?: number;
  width?: number;
  shippingFee?: number;
  weight?: number;
  active?: boolean;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Product implements IProduct {
  constructor(
    public id?: string,
    public name?: string,
    public qty?: number,
    public description?: string,
    public companyId?: string,
    public brand?: string,
    public categoryId?: string,
    public price?: number,
    public refurbished?: boolean,
    public height?: number,
    public depth?: number,
    public width?: number,
    public shippingFee?: number,
    public weight?: number,
    public active?: boolean,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {
    this.refurbished = this.refurbished || false;
    this.active = this.active || false;
  }
}
