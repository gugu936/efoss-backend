import { NgModule } from '@angular/core';
import { EfossSharedLibsModule } from './shared-libs.module';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { MenuItems } from './menu-items/menu-items';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';
@NgModule({
  imports: [EfossSharedLibsModule],
  declarations: [
    AccordionAnchorDirective,
    AlertComponent,
    AccordionLinkDirective,
    AlertErrorComponent,
    LoginModalComponent,
    AccordionDirective,
    HasAnyAuthorityDirective
  ],
  entryComponents: [LoginModalComponent],
  exports: [
    EfossSharedLibsModule,
    AccordionAnchorDirective,
    AlertComponent,
    AccordionDirective,
    AccordionLinkDirective,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective
  ],
  providers: [MenuItems]
})
export class EfossSharedModule {}
