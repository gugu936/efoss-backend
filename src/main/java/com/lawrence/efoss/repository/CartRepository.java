package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.Cart;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Cart entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CartRepository extends MongoRepository<Cart, String> {
    Optional<Cart> findOneByUserIdAndStatus(String userId, String status);
}
