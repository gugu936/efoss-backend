package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.Shipping;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Shipping entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShippingRepository extends MongoRepository<Shipping, String> {
    Optional<Shipping> findByOrderId(String orderId);
}
