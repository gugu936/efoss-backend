package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Order entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderRepository extends MongoRepository<Order, String> {
    Optional<List<Order>> findByIdIn(List<String> ids);

    Optional<List<Order>> findByCompanyId(String id);

    Optional<List<Order>> findByStatus(String status);
}
