package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.ProductImage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the ProductImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductImageRepository extends MongoRepository<ProductImage, String> {

    Optional<ProductImage> findByProductIdAndSequence(String id, Integer seq);

    Optional<List<ProductImage>> findByProductIdOrderBySequenceAsc(String id);
}
