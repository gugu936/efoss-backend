package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.CustomerOrder;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.List;

/**
 * Spring Data MongoDB repository for the CustomerOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerOrderRepository extends MongoRepository<CustomerOrder, String> {
    Optional<List<CustomerOrder>> findByUserId(String userId);

    Optional<CustomerOrder> findByCartId(String cartId);
}
