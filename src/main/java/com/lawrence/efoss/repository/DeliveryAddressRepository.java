package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.DeliveryAddress;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the DeliveryAddress entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeliveryAddressRepository extends MongoRepository<DeliveryAddress, String> {
    Optional<DeliveryAddress> findOneByUserId(String userId);
}
