package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.CartItem;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the CartItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CartItemRepository extends MongoRepository<CartItem, String> {
    Optional<List<CartItem>> findByCartId(String cartId);

    Optional<List<CartItem>> findByIdIn(List<String> ids);
}
