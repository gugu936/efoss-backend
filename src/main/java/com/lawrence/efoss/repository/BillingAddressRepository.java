package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.BillingAddress;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the BillingAddress entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BillingAddressRepository extends MongoRepository<BillingAddress, String> {
    Optional<BillingAddress> findOneByUserId(String userId);
}
