package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.ShippingMethod;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the ShippingMethod entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShippingMethodRepository extends MongoRepository<ShippingMethod, String> {
}
