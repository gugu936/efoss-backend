package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.EfossNotification;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Notification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationRepository extends MongoRepository<EfossNotification, String> {

    Optional<List<EfossNotification>> findByUserIdAndStatusOrderByCreatedAtDesc(String userId, String status);
}
