package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.Coin;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Coin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoinRepository extends MongoRepository<Coin, String> {
    Optional<List<Coin>> findByStatus(String status);
}
