package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends MongoRepository<Product, String> {
    Page<Product> findByCompanyId(Pageable pageable, String companyId);
    Optional<List<Product>> findByIdIn(List<String> ids);
    Page<Product> findByCompanyIdAndNameLike(Pageable pageable, String companyId, String name);

    Page<Product> findByCategoryIdAndActive(Pageable pageable, String id, Boolean active);
    Page<Product> findByCategoryIdAndNameLike(Pageable pageable, String id, String name);
}
