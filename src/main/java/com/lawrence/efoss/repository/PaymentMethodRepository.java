package com.lawrence.efoss.repository;

import com.lawrence.efoss.domain.PaymentMethod;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the PaymentMethod entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentMethodRepository extends MongoRepository<PaymentMethod, String> {
}
