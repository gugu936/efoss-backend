package com.lawrence.efoss.service.mapper;

import com.lawrence.efoss.config.DataUtil;
import com.lawrence.efoss.domain.Category;
import com.lawrence.efoss.domain.Company;
import com.lawrence.efoss.domain.Product;
import com.lawrence.efoss.repository.CategoryRepository;
import com.lawrence.efoss.repository.CompanyRepository;
import com.lawrence.efoss.service.dto.ProductDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductMapper {

    private final CategoryRepository categoryRepository;

    private final CompanyRepository companyRepository;

    public ProductMapper(CategoryRepository categoryRepository, CompanyRepository companyRepository) {
        this.categoryRepository = categoryRepository;
        this.companyRepository = companyRepository;
    }

    public List<ProductDTO> productsToProductDTOs(List<Product> products) {
        return products.stream()
            .filter(Objects::nonNull)
            .map(this::productToProductDTO)
            .collect(Collectors.toList());
    }

    public ProductDTO productToProductDTO(Product product) {
        ProductDTO productDTO = new ProductDTO();
        Optional<Company> optionalCompany = companyRepository.findById(product.getCompanyId());
        Optional<Category> categoryOptional = categoryRepository.findById(product.getCategoryId());

        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setQty(product.getQty());
        productDTO.setDescription(product.getDescription());
        productDTO.setBrand(product.getBrand());
        productDTO.setPrice(product.getPrice());
        productDTO.setHeight(product.getHeight());
        productDTO.setWidth(product.getWidth());
        productDTO.setSize(product.getSize() == null ? new ArrayList<>() : product.getSize());
        productDTO.setColor(product.getColor() == null ? new ArrayList<>() : product.getColor());
        productDTO.setDepth(product.getDepth());
        productDTO.setRefurbished(product.isRefurbished() == null ? false : product.isRefurbished());
        productDTO.setShippingFee(product.getShippingFee());
        productDTO.setWeight(product.getWeight());
        productDTO.setActive(product.isActive());
        productDTO.setCreatedAt(DataUtil.getDate(product.getCreatedAt().toString()));
        productDTO.setUpdatedAt(DataUtil.getDate(product.getUpdatedAt().toString()));
        optionalCompany.ifPresent(productDTO::setCompany);
        categoryOptional.ifPresent(productDTO::setCategory);

        return new ProductDTO(productDTO);
    }

    public List<Product> productDTOsToProducts(List<ProductDTO> productDTOs) {
        return productDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::productDTOToProduct)
            .collect(Collectors.toList());
    }

    public Product productDTOToProduct(ProductDTO productDTO) {
        if (productDTO == null) {
            return null;
        } else {
            Product product = new Product();

            product.setId(productDTO.getId());
            product.setName(productDTO.getName());
            product.setQty(productDTO.getQty());
            product.setWeight(productDTO.getWeight());
            product.setActive(productDTO.getActive());
            product.setShippingFee(productDTO.getShippingFee());
            product.setDescription(productDTO.getDescription());
            product.setCompanyId(productDTO.getCompany().getId());
            product.setBrand(productDTO.getBrand());
            product.setCategoryId(productDTO.getCategory().getId());
            product.setPrice(productDTO.getPrice());
            return product;
        }
    }
}
