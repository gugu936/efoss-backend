package com.lawrence.efoss.service.mapper;

import com.lawrence.efoss.config.DataUtil;
import com.lawrence.efoss.domain.Cart;
import com.lawrence.efoss.domain.CartItem;
import com.lawrence.efoss.repository.CartItemRepository;
import com.lawrence.efoss.service.dto.CartDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CartMapper {

    private final CartItemMapper cartItemMapper;

    private final CartItemRepository cartItemRepository;

    public CartMapper(CartItemMapper cartItemMapper, CartItemRepository cartItemRepository) {
        this.cartItemMapper = cartItemMapper;
        this.cartItemRepository = cartItemRepository;
    }

    public List<CartDTO> cartsToCartDTOs(List<Cart> carts) {
        return carts.stream()
            .filter(Objects::nonNull)
            .map(this::cartToCartDTO)
            .collect(Collectors.toList());
    }

    public CartDTO cartToCartDTO(Cart cart) {
        CartDTO cartDTO = new CartDTO();

        cartDTO.setId(cart.getId());
        cartDTO.setUserId(cart.getUserId());
        Optional<List<CartItem>> optionalCartItems = cartItemRepository.findByCartId(cart.getId());
        optionalCartItems.ifPresent(cartItems -> cartDTO.setCartItems(cartItemMapper.cartItemsToCartItemDTOs(cartItems)));
        cartDTO.setCreatedAt(DataUtil.getDate(cart.getCreatedAt().toString()));
        cartDTO.setUpdatedAt(DataUtil.getDate(cart.getUpdatedAt().toString()));
        cartDTO.setStatus(cart.getStatus());

        return new CartDTO(cartDTO);
    }

    public List<Cart> cartDTOsToCarts(List<CartDTO> cartDTOs) {
        return cartDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::cartDTOToCart)
            .collect(Collectors.toList());
    }

    public Cart cartDTOToCart(CartDTO cartDTO) {
        if (cartDTO == null) {
            return null;
        } else {
            Cart cart = new Cart();

            cart.setId(cartDTO.getId());
            cart.setUserId(cartDTO.getUserId());

            return cart;
        }
    }
}
