package com.lawrence.efoss.service.mapper;

import com.lawrence.efoss.config.DataUtil;
import com.lawrence.efoss.domain.Product;
import com.lawrence.efoss.domain.Wishlist;
import com.lawrence.efoss.repository.ProductRepository;
import com.lawrence.efoss.service.dto.WishlistDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WishlistMapper {

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    public WishlistMapper(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    public List<WishlistDTO> wishlistsToWishlistDTOs(List<Wishlist> wishlists) {
        return wishlists.stream()
            .filter(Objects::nonNull)
            .map(this::wishlistToWishlistDTO)
            .collect(Collectors.toList());
    }

    public WishlistDTO wishlistToWishlistDTO(Wishlist wishlist) {
        WishlistDTO wishlistDTO = new WishlistDTO();

        wishlistDTO.setId(wishlist.getId());
        wishlistDTO.setUserId(wishlist.getUserId());
        Optional<List<Product>> products = productRepository.findByIdIn(wishlist.getProductIds());
        products.ifPresent(productList -> wishlistDTO.setProducts(productMapper.productsToProductDTOs(productList)));

        wishlistDTO.setCreatedAt(DataUtil.getDate(wishlist.getCreatedAt().toString()));
        wishlistDTO.setUpdatedAt(DataUtil.getDate(wishlist.getUpdatedAt().toString()));

        return new WishlistDTO(wishlistDTO);
    }

    public List<Wishlist> wishlistDTOsToWishlists(List<WishlistDTO> wishlistDTOs) {
        return wishlistDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::wishlistDTOToWishlist)
            .collect(Collectors.toList());
    }

    public Wishlist wishlistDTOToWishlist(WishlistDTO wishlistDTO) {
        if (wishlistDTO == null) {
            return null;
        } else {
            Wishlist wishlist = new Wishlist();

            wishlist.setId(wishlistDTO.getId());
            wishlist.setUserId(wishlistDTO.getUserId());

            return wishlist;
        }
    }
}
