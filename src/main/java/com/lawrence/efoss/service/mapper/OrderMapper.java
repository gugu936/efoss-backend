package com.lawrence.efoss.service.mapper;

import com.lawrence.efoss.config.DataUtil;
import com.lawrence.efoss.domain.*;
import com.lawrence.efoss.repository.*;
import com.lawrence.efoss.service.dto.CartItemDTO;
import com.lawrence.efoss.service.dto.OrderDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderMapper {

    private final OrderRepository orderRepository;

    private final ProductRepository productRepository;

    private final CartRepository cartRepository;

    private final CartItemRepository cartItemRepository;

    private final PaymentMethodRepository paymentMethodRepository;

    private final ProductMapper productMapper;

    private final CartItemMapper cartItemMapper;

    private final CompanyRepository companyRepository;

    private final ShippingRepository shippingRepository;

    private final ShippingMethodRepository shippingMethodRepository;

    private final CartMapper cartMapper;
    public OrderMapper(OrderRepository orderRepository, ProductRepository productRepository, CartRepository cartRepository, CartItemRepository cartItemRepository, PaymentMethodRepository paymentMethodRepository, ProductMapper productMapper, CartItemMapper cartItemMapper, CompanyRepository companyRepository, ShippingRepository shippingRepository, ShippingMethodRepository shippingMethodRepository, CartMapper cartMapper) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.cartRepository = cartRepository;
        this.cartItemRepository = cartItemRepository;
        this.paymentMethodRepository = paymentMethodRepository;
        this.productMapper = productMapper;
        this.cartItemMapper = cartItemMapper;
        this.companyRepository = companyRepository;
        this.shippingRepository = shippingRepository;
        this.shippingMethodRepository = shippingMethodRepository;
        this.cartMapper = cartMapper;
    }

    public List<OrderDTO> ordersToOrderDTOs(List<Order> orders) {
        return orders.stream()
            .filter(Objects::nonNull)
            .map(this::orderToOrderDTO)
            .collect(Collectors.toList());
    }

    public OrderDTO orderToOrderDTO(Order order) {
        OrderDTO orderDTO = new OrderDTO();

        Optional<PaymentMethod> optionalPaymentMethod = paymentMethodRepository.findById(order.getPaymentMethodId());

        if (StringUtils.isNotBlank(order.getShippingMethodId())) {
            Optional<ShippingMethod> optionalShippingMethod  = shippingMethodRepository.findById(order.getShippingMethodId());
            optionalShippingMethod.ifPresent(orderDTO::setShippingMethod);
        }

        Optional<List<CartItem>> optionalCartItems = cartItemRepository.findByIdIn(order.getCartItemIds());

        Optional<Shipping> optionalShipping = shippingRepository.findByOrderId(order.getId());
        optionalShipping.ifPresent(orderDTO::setShipping);

        List<CartItemDTO> itemDTOS = new ArrayList<>();
        if (optionalCartItems.isPresent()) {
            itemDTOS = cartItemMapper.cartItemsToCartItemDTOs(optionalCartItems.get());
        }

        List<CartItemDTO> itemDTOList = new ArrayList<>();
        for (CartItemDTO dto : itemDTOS) {
            if (dto.getProduct().getCompany().getId().equals(order.getCompanyId())) {
                itemDTOList.add(dto);
            }
        }

        Optional<Company> companyOptional = companyRepository.findById(order.getCompanyId());

        companyOptional.ifPresent(orderDTO::setCompany);
        optionalPaymentMethod.ifPresent(orderDTO::setPaymentMethod);

        orderDTO.setId(order.getId());
        orderDTO.setCartItems(itemDTOList);
        orderDTO.setStatus(order.getStatus());
        orderDTO.setTotal(order.getTotal());
        orderDTO.setCartId(order.getCartId());
        orderDTO.setCreatedAt(DataUtil.getDate(order.getCreatedAt().toString()));
        orderDTO.setUpdatedAt(DataUtil.getDate(order.getUpdatedAt().toString()));
        return new OrderDTO(orderDTO);
    }

    public List<Order> orderDTOsToOrders(List<OrderDTO> orderDTOs) {
        return orderDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::orderDTOToOrder)
            .collect(Collectors.toList());
    }

    public Order orderDTOToOrder(OrderDTO orderDTO) {
        if (orderDTO == null) {
            return null;
        } else {
            Order order = new Order();

            order.setId(orderDTO.getId());
            order.setCompanyId(orderDTO.getCompany().getId());
            order.setStatus(orderDTO.getStatus());
            order.setShippingMethodId(orderDTO.getShippingMethod().getId());
            order.setPaymentMethodId(orderDTO.getPaymentMethod().getId());
            order.setTotal(orderDTO.getTotal());

            return order;
        }
    }
}
