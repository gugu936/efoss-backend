package com.lawrence.efoss.service.mapper;

import com.lawrence.efoss.config.DataUtil;
import com.lawrence.efoss.domain.CartItem;
import com.lawrence.efoss.domain.Product;
import com.lawrence.efoss.repository.ProductRepository;
import com.lawrence.efoss.service.dto.CartItemDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CartItemMapper {
    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    public CartItemMapper(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    public List<CartItemDTO> cartItemsToCartItemDTOs(List<CartItem> cartItems) {
        return cartItems.stream()
            .filter(Objects::nonNull)
            .map(this::cartItemToCartItemDTO)
            .collect(Collectors.toList());
    }

    public CartItemDTO cartItemToCartItemDTO(CartItem cartItem) {
        CartItemDTO cartItemDTO = new CartItemDTO();
        Optional<Product> optionalProduct = productRepository.findById(cartItem.getProductId());

        cartItemDTO.setId(cartItem.getId());
        cartItemDTO.setCartId(cartItem.getCartId());
        cartItemDTO.setQty(cartItem.getQty());
        cartItemDTO.setPrice(cartItem.getPrice());
        cartItemDTO.setCreatedAt(DataUtil.getDate(cartItem.getCreatedAt().toString()));
        cartItemDTO.setUpdatedAt(DataUtil.getDate(cartItem.getUpdatedAt().toString()));
        cartItemDTO.setColor(cartItem.getColor());
        cartItemDTO.setSize(cartItem.getSize());
        optionalProduct.ifPresent(product -> cartItemDTO.setProduct(productMapper.productToProductDTO(product)));

        return new CartItemDTO(cartItemDTO);
    }

    public List<CartItem> cartItemDTOsToCartItems(List<CartItemDTO> cartItemDTOs) {
        return cartItemDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::cartItemDTOToCartItem)
            .collect(Collectors.toList());
    }

    public CartItem cartItemDTOToCartItem(CartItemDTO cartItemDTO) {
        if (cartItemDTO == null) {
            return null;
        } else {
            CartItem cartItem = new CartItem();

            cartItem.setId(cartItemDTO.getId());
            cartItem.setCartId(cartItemDTO.getCartId());
            cartItem.setQty(cartItemDTO.getQty());
            cartItem.setProductId(cartItemDTO.getProduct().getId());
            cartItem.setPrice(cartItemDTO.getPrice());
            return cartItem;
        }
    }
}
