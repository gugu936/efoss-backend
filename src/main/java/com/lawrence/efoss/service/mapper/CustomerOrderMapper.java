package com.lawrence.efoss.service.mapper;

import com.lawrence.efoss.config.DataUtil;
import com.lawrence.efoss.domain.*;
import com.lawrence.efoss.repository.*;
import com.lawrence.efoss.service.dto.CustomerOrderDTO;
import com.lawrence.efoss.service.dto.OrderDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CustomerOrderMapper {

    private final Logger log = LoggerFactory.getLogger(CustomerOrderMapper.class);

    private final CartRepository cartRepository;

    private final CartMapper cartMapper;

    private final OrderMapper orderMapper;

    private final PaymentMethodRepository paymentMethodRepository;

    private final OrderRepository orderRepository;

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    private final CompanyRepository companyRepository;

    private final DeliveryAddressRepository deliveryAddressRepository;

    private final BillingAddressRepository billingAddressRepository;

    private final ShippingMethodRepository shippingMethodRepository;

    public CustomerOrderMapper(CartRepository cartRepository, CartMapper cartMapper, OrderMapper orderMapper, PaymentMethodRepository paymentMethodRepository, OrderRepository orderRepository, UserRepository userRepository, UserMapper userMapper, CompanyRepository companyRepository, DeliveryAddressRepository deliveryAddressRepository, BillingAddressRepository billingAddressRepository, ShippingMethodRepository shippingMethodRepository) {
        this.cartRepository = cartRepository;
        this.cartMapper = cartMapper;
        this.orderMapper = orderMapper;
        this.paymentMethodRepository = paymentMethodRepository;
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.companyRepository = companyRepository;
        this.deliveryAddressRepository = deliveryAddressRepository;
        this.billingAddressRepository = billingAddressRepository;
        this.shippingMethodRepository = shippingMethodRepository;
    }

    public List<CustomerOrderDTO> customerOrdersToCustomerOrderDTOs(List<CustomerOrder> customerOrders) {
        return customerOrders.stream()
            .filter(Objects::nonNull)
            .map(this::customerOrderToCustomerOrderDTO)
            .collect(Collectors.toList());
    }

    public CustomerOrderDTO customerOrderToCustomerOrderDTO(CustomerOrder customerOrder) {
        CustomerOrderDTO customerOrderDTO = new CustomerOrderDTO();

        // Optional<Cart> optionalCart = cartRepository.findById(customerOrder.getCartId());
        // optionalCart.ifPresent(cart -> customerOrderDTO.setCart(cartMapper.cartToCartDTO(cart)));
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        Optional<BillingAddress> optionalBillingAddress = billingAddressRepository.findById(customerOrder.getBillingAddressId());
        if (optionalBillingAddress.isPresent() && customerOrder.getSameAsBilling()) {
            deliveryAddress = DataUtil.deliveryAddress(optionalBillingAddress.get());
        } else {
            Optional<DeliveryAddress> optionalDeliveryAddress = deliveryAddressRepository.findById(customerOrder.getDeliveryAddressId());
            if (optionalDeliveryAddress.isPresent()) {
                deliveryAddress = optionalDeliveryAddress.get();
            }
        }

        Optional<PaymentMethod> optionalPaymentMethod = paymentMethodRepository.findById(customerOrder.getPaymentMethodId());
        optionalPaymentMethod.ifPresent(customerOrderDTO::setPaymentMethod);

        Optional<ShippingMethod> optionalShippingMethod = shippingMethodRepository.findById(customerOrder.getShippingMethodId());
        optionalShippingMethod.ifPresent(customerOrderDTO::setShippingMethod);

        Map<Company, List<OrderDTO>> map = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : customerOrder.getOrderIds().entrySet()) {
            Optional<List<Order>> optionalOrders = orderRepository.findByIdIn(entry.getValue());
            optionalOrders.ifPresent(orders -> map.put(companyRepository.findById(entry.getKey()).orElseGet(null), orderMapper.ordersToOrderDTOs(orders)));
        }
        customerOrderDTO.setOrders(map);

        log.debug("userId is {}", customerOrder.getUserId());
        Optional<User> optionalUser = userRepository.findById(customerOrder.getUserId());
        optionalUser.ifPresent(user -> customerOrderDTO.setUser(userMapper.userToUserDTO(user)));
        optionalBillingAddress.ifPresent(customerOrderDTO::setBillingAddress);

        customerOrderDTO.setUseEWallet(customerOrder.getUseEWallet());
        customerOrderDTO.setDeliveryAddress(deliveryAddress);
        customerOrderDTO.setCartId(customerOrder.getCartId());
        customerOrderDTO.setId(customerOrder.getId());
        customerOrderDTO.setStatus(customerOrder.getStatus());
        customerOrderDTO.setDiscount(customerOrder.getDiscount());
        customerOrderDTO.setSameAsBilling(customerOrder.getSameAsBilling());
        customerOrderDTO.setTotal(customerOrder.getTotal());
        customerOrderDTO.setCreatedAt(DataUtil.getDate(customerOrder.getCreatedAt().toString()));
        customerOrderDTO.setUpdatedAt(DataUtil.getDate(customerOrder.getUpdatedAt().toString()));

        return new CustomerOrderDTO(customerOrderDTO);
    }

    public List<CustomerOrder> customerOrderDTOsToCustomerOrders(List<CustomerOrderDTO> customerOrderDTOs) {
        return customerOrderDTOs.stream()
            .filter(Objects::nonNull)
            .map(this::customerOrderDTOToCustomerOrder)
            .collect(Collectors.toList());
    }

    public CustomerOrder customerOrderDTOToCustomerOrder(CustomerOrderDTO customerOrderDTO) {
        if (customerOrderDTO == null) {
            return null;
        } else {
            CustomerOrder customerOrder = new CustomerOrder();

            customerOrder.setId(customerOrderDTO.getId());
            customerOrder.setCartId(customerOrderDTO.getCartId());
            customerOrder.setStatus(customerOrderDTO.getStatus());
            Map<String, List<String>> map = new HashMap<>();
            for (Map.Entry<Company, List<OrderDTO>> entry : customerOrderDTO.getOrders().entrySet()) {
                map.put(entry.getKey().getId(), entry.getValue().stream().map(OrderDTO::getId).collect(Collectors.toList()));
            }
            customerOrder.setOrderIds(map);
            customerOrder.setPaymentMethodId(customerOrderDTO.getPaymentMethod().getId());
            customerOrder.setDiscount(customerOrderDTO.getDiscount());
            customerOrder.setSameAsBilling(customerOrderDTO.getSameAsBilling());
            customerOrder.setTotal(customerOrderDTO.getTotal());
            customerOrder.setUserId(customerOrderDTO.getUser().getId());
            return customerOrder;
        }
    }
}
