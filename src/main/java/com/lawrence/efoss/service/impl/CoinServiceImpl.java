package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.service.CoinService;
import com.lawrence.efoss.domain.Coin;
import com.lawrence.efoss.repository.CoinRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Coin}.
 */
@Service
public class CoinServiceImpl implements CoinService {

    private final Logger log = LoggerFactory.getLogger(CoinServiceImpl.class);

    private final CoinRepository coinRepository;

    public CoinServiceImpl(CoinRepository coinRepository) {
        this.coinRepository = coinRepository;
    }

    /**
     * Save a coin.
     *
     * @param coin the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Coin save(Coin coin) {
        log.debug("Request to save Coin : {}", coin);
        return coinRepository.save(coin);
    }

    /**
     * Get all the coins.
     *
     * @return the list of entities.
     */
    @Override
    public List<Coin> findAll() {
        log.debug("Request to get all Coins");
        return coinRepository.findAll();
    }

    /**
     * Get one coin by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<Coin> findOne(String id) {
        log.debug("Request to get Coin : {}", id);
        return coinRepository.findById(id);
    }

    /**
     * Delete the coin by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Coin : {}", id);
        coinRepository.deleteById(id);
    }
}
