package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.domain.DeliveryAddress;
import com.lawrence.efoss.domain.User;
import com.lawrence.efoss.repository.DeliveryAddressRepository;
import com.lawrence.efoss.repository.UserRepository;
import com.lawrence.efoss.security.SecurityUtils;
import com.lawrence.efoss.service.DeliveryAddressService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DeliveryAddress}.
 */
@Service
public class DeliveryAddressServiceImpl implements DeliveryAddressService {

    private final Logger log = LoggerFactory.getLogger(DeliveryAddressServiceImpl.class);

    private final DeliveryAddressRepository deliveryAddressRepository;

    private final UserRepository userRepository;

    public DeliveryAddressServiceImpl(DeliveryAddressRepository deliveryAddressRepository, UserRepository userRepository) {
        this.deliveryAddressRepository = deliveryAddressRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a deliveryAddress.
     *
     * @param deliveryAddress the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DeliveryAddress save(DeliveryAddress deliveryAddress) {
        log.debug("Request to save DeliveryAddress : {}", deliveryAddress);
        return deliveryAddressRepository.save(deliveryAddress);
    }

    /**
     * Get all the deliveryAddresses.
     *
     * @return the list of entities.
     */
    @Override
    public List<DeliveryAddress> findAll() {
        log.debug("Request to get all DeliveryAddresses");
        return deliveryAddressRepository.findAll();
    }

    /**
     * Get one deliveryAddress by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<DeliveryAddress> findOne(String id) {
        log.debug("Request to get DeliveryAddress : {}", id);
        return deliveryAddressRepository.findById(id);
    }

    /**
     * Delete the deliveryAddress by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete DeliveryAddress : {}", id);
        deliveryAddressRepository.deleteById(id);
    }

    @Override
    public Optional<DeliveryAddress> getCustomerDeliveryAddress() {
        String login = SecurityUtils.getCurrentUserLogin().orElse("");
        if (StringUtils.isNotBlank(login)) {
            Optional<User> optionalUser = userRepository.findOneByLogin(login);
            if (optionalUser.isPresent()) {
                Optional<DeliveryAddress> optionalDeliveryAddress = deliveryAddressRepository.findOneByUserId(optionalUser.get().getId());
                if (optionalDeliveryAddress.isPresent()) {
                    return optionalDeliveryAddress;
                } else {
                    DeliveryAddress deliveryAddress = new DeliveryAddress();
                    ZonedDateTime now = ZonedDateTime.now();
                    deliveryAddress.setCreatedAt(now);
                    deliveryAddress.setUserId(optionalUser.get().getId());
                    deliveryAddress.setUpdatedAt(now);
                    deliveryAddressRepository.save(deliveryAddress);
                    return Optional.of(deliveryAddress);
                }
            }
        }
        return Optional.empty();
    }
}
