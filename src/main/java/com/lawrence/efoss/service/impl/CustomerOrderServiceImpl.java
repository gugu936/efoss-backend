package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.domain.*;
import com.lawrence.efoss.repository.*;
import com.lawrence.efoss.security.SecurityUtils;
import com.lawrence.efoss.service.CustomerOrderService;
import com.lawrence.efoss.service.OrderService;
import com.lawrence.efoss.service.Web3jService;
import com.lawrence.efoss.service.dto.CartDTO;
import com.lawrence.efoss.service.dto.CartItemDTO;
import com.lawrence.efoss.service.dto.CustomerOrderDTO;
import com.lawrence.efoss.service.dto.ProductDTO;
import com.lawrence.efoss.service.mapper.CartMapper;
import com.lawrence.efoss.service.mapper.CustomerOrderMapper;
import com.lawrence.efoss.web.rest.UserResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Service Implementation for managing {@link CustomerOrder}.
 */
@Service
public class CustomerOrderServiceImpl implements CustomerOrderService {

    private final Logger log = LoggerFactory.getLogger(CustomerOrderServiceImpl.class);

    private final CustomerOrderRepository customerOrderRepository;

    private final UserRepository userRepository;

    private final CustomerOrderMapper customerOrderMapper;

    private final CartRepository cartRepository;

    private final OrderService orderService;

    private final CartMapper cartMapper;

    private final ShippingMethodRepository shippingMethodRepository;

    private final String E_WAY = "Self Pick Up";

    private final Web3jService web3jService;

    private final CoinRepository coinRepository;

    private final UserResource userResource;

    private final double REFURBISHED_BASE = 0.1;

    private final double SHIP_BASE = 0.05;

    private final ProductRepository productRepository;

    public CustomerOrderServiceImpl(CustomerOrderRepository customerOrderRepository, UserRepository userRepository, CustomerOrderMapper customerOrderMapper, CartRepository cartRepository, OrderService orderService, CartMapper cartMapper, ShippingMethodRepository shippingMethodRepository, Web3jService web3jService, CoinRepository coinRepository, UserResource userResource, ProductRepository productRepository) {
        this.customerOrderRepository = customerOrderRepository;
        this.userRepository = userRepository;
        this.customerOrderMapper = customerOrderMapper;
        this.cartRepository = cartRepository;
        this.orderService = orderService;
        this.cartMapper = cartMapper;
        this.shippingMethodRepository = shippingMethodRepository;
        this.web3jService = web3jService;
        this.coinRepository = coinRepository;
        this.userResource = userResource;
        this.productRepository = productRepository;
    }

    /**
     * Save a customerOrder.
     *
     * @param customerOrder the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CustomerOrder save(CustomerOrder customerOrder) {
        ZonedDateTime now = ZonedDateTime.now();
        if (customerOrder.getCreatedAt() == null) {
            customerOrder.setCreatedAt(now);
        }
        customerOrder.setUpdatedAt(now);

        log.debug("Request to save CustomerOrder : {}", customerOrder);
        customerOrder.setStatus("2");
        String cartId = customerOrder.getCartId();
        Optional<Cart> optionalCart = cartRepository.findById(cartId);
        if (optionalCart.isPresent()) {
            Cart cart = optionalCart.get();
            cart.setStatus("closed");
            cartRepository.save(cart);
            CartDTO cartDTO = cartMapper.cartToCartDTO(cart);
            Map<String, List<CartItemDTO>> companyProducts = new HashMap<>();
            double orderTotal = 0;
            for (CartItemDTO itemDTO : cartDTO.getCartItems()) {
                // itemDTO.getProduct().getId();
                Optional<Product> optionalProduct = productRepository.findById(itemDTO.getProduct().getId());
                if (optionalProduct.isPresent()) {
                    Product product = optionalProduct.get();
                    product.setQty(product.getQty() - itemDTO.getQty());
                    productRepository.save(product);
                };
                ProductDTO dto = itemDTO.getProduct();
                String companyId = dto.getCompany().getId();
                if (companyProducts.get(companyId) == null) {
                    List<CartItemDTO> itemDTOS = new ArrayList<>();
                    itemDTOS.add(itemDTO);
                    companyProducts.put(companyId, itemDTOS);
                } else {
                    List<CartItemDTO> itemDTOS = companyProducts.get(companyId);
                    itemDTOS.add(itemDTO);
                    companyProducts.put(companyId, itemDTOS);
                }
                orderTotal += itemDTO.getPrice().doubleValue();
            }
            Map<String, List<String>> ids = new HashMap<>();
            // group by company id
            for (Map.Entry<String, List<CartItemDTO>> entry : companyProducts.entrySet()) {
                Order order = new Order();
                order.setCartId(cartId);

                List<String> cartItemIds = new ArrayList<>();
                double total = 0;
                double ship = 0;
                for (CartItemDTO cartItemDTO : entry.getValue()) {
                    cartItemIds.add(cartItemDTO.getId());
                    total += cartItemDTO.getPrice().doubleValue();
                    BigDecimal fee = cartItemDTO.getProduct().getShippingFee();
                    if (fee != null && ship == 0) {
                        ship = cartItemDTO.getProduct().getShippingFee().doubleValue();
                    } else if (fee != null && fee.doubleValue() > ship) {
                        ship = fee.doubleValue();
                    }
                    orderTotal += ship;
                }

                order.setCartItemIds(cartItemIds);
                order.setStatus("2");
                order.setPaymentMethodId(customerOrder.getPaymentMethodId());
                order.setShippingMethodId(customerOrder.getShippingMethodId());
                order.setTotal(BigDecimal.valueOf(total));
                order.setCompanyId(entry.getKey());
                order = orderService.save(order);

                if (ids.get(entry.getKey()) != null) {
                    List<String> IDS = ids.get(entry.getKey());
                    IDS.add(order.getId());
                    ids.put(entry.getKey(), IDS);
                } else {
                    List<String> IDS = new ArrayList<>();
                    IDS.add(order.getId());
                    ids.put(entry.getKey(), IDS);
                }
            }
            customerOrder.setOrderIds(ids);


            customerOrder.setTotal(BigDecimal.valueOf(orderTotal));
            // CustomerOrderDTO customerOrderDTO = customerOrderMapper.customerOrderToCustomerOrderDTO(customerOrder);
            Optional<User> optionalUser = userRepository.findById(customerOrder.getUserId());
            if (optionalUser.isPresent()) {
                if (customerOrder.getUseEWallet()) {
                    Coin coin = new Coin();
                    coin.setUserId(optionalUser.get().getId());
                    coin.setStatus("0");
                    coin.setChange("0");
                    BigDecimal balance = userResource.getUserBalance();
                    int gas = 0;
                    if (orderTotal > (balance.doubleValue() - gas) && balance.doubleValue() != 0) {
                        BigDecimal amount = BigDecimal.valueOf(balance.doubleValue() - gas);
                        customerOrder.setDiscount(amount);
                        coin.setAmount(amount);
                    } else if (orderTotal < (balance.doubleValue() - gas)) {
                        BigDecimal amount = BigDecimal.valueOf(balance.doubleValue() - orderTotal);
                        coin.setAmount(amount);
                        customerOrder.setDiscount(amount);
                    }
                    coinRepository.save(coin);
                }
                Optional<ShippingMethod> shippingMethod = shippingMethodRepository.findById(customerOrder.getShippingMethodId());
                String shipName = "-";
                if (shippingMethod.isPresent()) {
                    shipName = shippingMethod.get().getName();
                }
                calculation(cartDTO, optionalUser.get(), shipName.equals(E_WAY));
            }

        }
        customerOrderRepository.save(customerOrder);
        return customerOrder;
    }

    public void calculation(CartDTO cartDTO, User user, Boolean eWay) {

        double refurbished = 0;
        double nonRefurbished = 0;
        for (CartItemDTO itemDTO : cartDTO.getCartItems()) {
            if (itemDTO.getProduct().getRefurbished()) {
                refurbished += itemDTO.getPrice().doubleValue();
            } else {
                nonRefurbished += itemDTO.getPrice().doubleValue();
            }
        }
        double point1 = refurbished * REFURBISHED_BASE;
        double point2 = nonRefurbished * SHIP_BASE;

        log.debug("point1 point2 {} {}", point1, point2);

        double ePoint = user.getePoint() == null ? 0 : user.getePoint().doubleValue();
        double updated;
        Coin coin = new Coin();
        coin.setUserId(user.getId());
        coin.setStatus("0");
        if (eWay) {
            coin.setChange("1");
            updated = (ePoint + point1 + point2);
            coin.setAmount(BigDecimal.valueOf(point1 + point2));
        } else {
            if ((point1 - point2) > 0) {
                coin.setAmount(BigDecimal.valueOf(point1 - point2));
                coin.setChange("1");
            } else {
                // if user ePoint not enough
                if (ePoint != 0) {
                    if ((ePoint - (point1 - point2)) < 0) {
                        coin.setAmount(BigDecimal.valueOf(ePoint));
                    } else {
                        coin.setAmount(BigDecimal.valueOf(point1 - point2));
                    }
                    coin.setChange("0");
                }
                coin.setStatus("1");
            }
            updated = (int) (ePoint + point1 - point2);
        }
        coinRepository.save(coin);
        if (updated < 0) {
            updated = 0;
        }
        user.setePoint(BigDecimal.valueOf(updated));
        userRepository.save(user);
    }

    @Scheduled(fixedDelay = 1000 * 10)
    public void syncCoin() {
        Optional<List<Coin>> coinOptional = coinRepository.findByStatus("0");
        if (coinOptional.isPresent()) {
            log.debug("coinOptional size {}", coinOptional.get().size());
            for (Coin coin : coinOptional.get()) {
                Optional<User> optionalUser = userRepository.findById(coin.getUserId());
                if (optionalUser.isPresent()) {
                    if (coin.getChange().equals("0")) {
                        web3jService.decrease(optionalUser.get(), coin.getAmount().doubleValue());
                    } else {
                        web3jService.increase(optionalUser.get(), coin.getAmount().doubleValue());
                    }
                    coin.setStatus("1");
                    coinRepository.save(coin);
                }
            }
        }
    }


    /**
     * Get all the customerOrders.
     *
     * @return the list of entities.
     */
    @Override
    public List<CustomerOrder> findAll() {
        log.debug("Request to get all CustomerOrders");
        return customerOrderRepository.findAll();
    }

    /**
     * Get one customerOrder by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<CustomerOrder> findOne(String id) {
        log.debug("Request to get CustomerOrder : {}", id);
        return customerOrderRepository.findById(id);
    }

    @Override
    public List<CustomerOrderDTO> getUserAllOrders() {
        List<CustomerOrderDTO> list = new ArrayList<>();
        String login = SecurityUtils.getCurrentUserLogin().orElse("");
        Optional<User> userOptional = userRepository.findOneByLogin(login);
        if (userOptional.isPresent()) {
            Optional<List<CustomerOrder>> optionalCustomerOrders = customerOrderRepository.findByUserId(userOptional.get().getId());
            if (optionalCustomerOrders.isPresent()) {
                return customerOrderMapper.customerOrdersToCustomerOrderDTOs(optionalCustomerOrders.get());
            }
        }
        return list;
    }

    @Override
    public CustomerOrderDTO getOrderByCartId(String cartId) {
        CustomerOrderDTO customerOrderDTO = new CustomerOrderDTO();
        Optional<CustomerOrder> optional = customerOrderRepository.findByCartId(cartId);
        if (optional.isPresent()) {
            customerOrderDTO = customerOrderMapper.customerOrderToCustomerOrderDTO(optional.get());
        }
        return customerOrderDTO;
    }

    /**
     * Delete the customerOrder by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete CustomerOrder : {}", id);
        customerOrderRepository.deleteById(id);
    }
}
