package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.service.CartItemService;
import com.lawrence.efoss.domain.CartItem;
import com.lawrence.efoss.repository.CartItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link CartItem}.
 */
@Service
public class CartItemServiceImpl implements CartItemService {

    private final Logger log = LoggerFactory.getLogger(CartItemServiceImpl.class);

    private final CartItemRepository cartItemRepository;

    public CartItemServiceImpl(CartItemRepository cartItemRepository) {
        this.cartItemRepository = cartItemRepository;
    }

    /**
     * Save a cartItem.
     *
     * @param cartItem the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CartItem save(CartItem cartItem) {
        log.debug("Request to save CartItem : {}", cartItem);
        ZonedDateTime now = ZonedDateTime.now();
        if (cartItem.getCreatedAt() == null) {
            cartItem.setCreatedAt(now);
        }
        cartItem.setUpdatedAt(now);
        return cartItemRepository.save(cartItem);
    }

    /**
     * Get all the cartItems.
     *
     * @return the list of entities.
     */
    @Override
    public List<CartItem> findAll() {
        log.debug("Request to get all CartItems");
        return cartItemRepository.findAll();
    }

    /**
     * Get one cartItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<CartItem> findOne(String id) {
        log.debug("Request to get CartItem : {}", id);
        return cartItemRepository.findById(id);
    }

    /**
     * Delete the cartItem by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete CartItem : {}", id);
        cartItemRepository.deleteById(id);
    }
}
