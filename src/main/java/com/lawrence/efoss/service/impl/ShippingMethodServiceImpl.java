package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.service.ShippingMethodService;
import com.lawrence.efoss.domain.ShippingMethod;
import com.lawrence.efoss.repository.ShippingMethodRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ShippingMethod}.
 */
@Service
public class ShippingMethodServiceImpl implements ShippingMethodService {

    private final Logger log = LoggerFactory.getLogger(ShippingMethodServiceImpl.class);

    private final ShippingMethodRepository shippingMethodRepository;

    public ShippingMethodServiceImpl(ShippingMethodRepository shippingMethodRepository) {
        this.shippingMethodRepository = shippingMethodRepository;
    }

    /**
     * Save a shippingMethod.
     *
     * @param shippingMethod the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ShippingMethod save(ShippingMethod shippingMethod) {
        log.debug("Request to save ShippingMethod : {}", shippingMethod);
        return shippingMethodRepository.save(shippingMethod);
    }

    /**
     * Get all the shippingMethods.
     *
     * @return the list of entities.
     */
    @Override
    public List<ShippingMethod> findAll() {
        log.debug("Request to get all ShippingMethods");
        return shippingMethodRepository.findAll();
    }

    /**
     * Get one shippingMethod by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<ShippingMethod> findOne(String id) {
        log.debug("Request to get ShippingMethod : {}", id);
        return shippingMethodRepository.findById(id);
    }

    /**
     * Delete the shippingMethod by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete ShippingMethod : {}", id);
        shippingMethodRepository.deleteById(id);
    }
}
