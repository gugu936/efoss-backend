package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.service.ShippingService;
import com.lawrence.efoss.domain.Shipping;
import com.lawrence.efoss.repository.ShippingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Shipping}.
 */
@Service
public class ShippingServiceImpl implements ShippingService {

    private final Logger log = LoggerFactory.getLogger(ShippingServiceImpl.class);

    private final ShippingRepository shippingRepository;

    public ShippingServiceImpl(ShippingRepository shippingRepository) {
        this.shippingRepository = shippingRepository;
    }

    /**
     * Save a shipping.
     *
     * @param shipping the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Shipping save(Shipping shipping) {
        ZonedDateTime now = ZonedDateTime.now();
        if (shipping.getCreatedAt() == null) {
            shipping.setCreatedAt(now);
        }
        shipping.setUpdatedAt(now);
        log.debug("Request to save Shipping : {}", shipping);
        return shippingRepository.save(shipping);
    }

    /**
     * Get all the shippings.
     *
     * @return the list of entities.
     */
    @Override
    public List<Shipping> findAll() {
        log.debug("Request to get all Shippings");
        return shippingRepository.findAll();
    }

    /**
     * Get one shipping by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<Shipping> findOne(String id) {
        log.debug("Request to get Shipping : {}", id);
        return shippingRepository.findById(id);
    }

    /**
     * Delete the shipping by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Shipping : {}", id);
        shippingRepository.deleteById(id);
    }
}
