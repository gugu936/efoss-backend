package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.config.DataUtil;
import com.lawrence.efoss.domain.*;
import com.lawrence.efoss.repository.CustomerOrderRepository;
import com.lawrence.efoss.repository.OrderRepository;
import com.lawrence.efoss.repository.UserRepository;
import com.lawrence.efoss.security.SecurityUtils;
import com.lawrence.efoss.service.OrderService;
import com.lawrence.efoss.service.dto.CustomerOrderDTO;
import com.lawrence.efoss.service.dto.OrderDTO;
import com.lawrence.efoss.service.mapper.CustomerOrderMapper;
import com.lawrence.efoss.service.mapper.OrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.*;

/**
 * Service Implementation for managing {@link Order}.
 */
@Service
public class OrderServiceImpl implements OrderService {

    private final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

    private final OrderRepository orderRepository;

    private final OrderMapper orderMapper;

    private final UserRepository userRepository;

    private final CustomerOrderRepository customerOrderRepository;


    private final MongoTemplate template;

    private final CompanyServiceImpl companyService;

    private final CustomerOrderMapper customerOrderMapper;

    public OrderServiceImpl(OrderRepository orderRepository, OrderMapper orderMapper, UserRepository userRepository, CustomerOrderRepository customerOrderRepository, MongoTemplate template, CompanyServiceImpl companyService, CustomerOrderMapper customerOrderMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.userRepository = userRepository;
        this.customerOrderRepository = customerOrderRepository;
        this.template = template;
        this.companyService = companyService;
        this.customerOrderMapper = customerOrderMapper;
    }

    /**
     * Save a order.
     *
     * @param order the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Order save(Order order) {
        log.debug("Request to save Order : {}", order);
        ZonedDateTime now = ZonedDateTime.now();
        if (order.getCreatedAt() == null) {
            order.setCreatedAt(now);
        }
        order.setUpdatedAt(now);
        return orderRepository.save(order);
    }

    @Override
    public Page<OrderDTO> searchOrders(Pageable pageable, String keyword) {
        String companyId = companyService.getCompanyIdByUsername();
        List<OrderDTO> orderDTOS = orderMapper.ordersToOrderDTOs(search(companyId, keyword));
        Map<String, Object> res = get(orderDTOS.get(0).getCartId());
        for (OrderDTO orderDTO : orderDTOS) {
            orderDTO.setBillingAddress((BillingAddress) res.get("billingAddress"));
            orderDTO.setDeliveryAddress((DeliveryAddress) res.get("deliveryAddress"));
        }
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), orderDTOS.size());
        Page<OrderDTO> page = new PageImpl<>(orderDTOS.subList(start, end), pageable, orderDTOS.size());
        log.debug("orderDTOS {}", orderDTOS.get(0).getBillingAddress().getId());
        return page;
    }

    private Map<String, Object> get (String cartId) {
        Map<String, Object> result = new HashMap<>();
        Optional<CustomerOrder> optionalCustomerOrder = customerOrderRepository.findByCartId(cartId);

        BillingAddress billingAddress = new BillingAddress();
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        if (optionalCustomerOrder.isPresent()) {
            CustomerOrderDTO cartToCartDTO = customerOrderMapper.customerOrderToCustomerOrderDTO(optionalCustomerOrder.get());
            billingAddress = cartToCartDTO.getBillingAddress();
            if (cartToCartDTO.getSameAsBilling()) {
                deliveryAddress = DataUtil.deliveryAddress(billingAddress);
            } else {
                deliveryAddress = cartToCartDTO.getDeliveryAddress();
            }
        }
        result.put("billingAddress" ,billingAddress);
        result.put("deliveryAddress" ,deliveryAddress);
        return result;
    }

    private List<Order> search(String value,  String key) {
        Query query = new Query();
        query.addCriteria(Criteria.where("company_id").is(value));
        Criteria criteria = new Criteria();
        criteria.orOperator(Criteria.where("_id").regex(".*"+key+".*", "i")
       //     ,Criteria.where("brand").regex(".*"+key+".*", "i")
         //   ,Criteria.where("description").regex(".*"+key+".*", "i")
        );
        query.addCriteria(criteria);
        List<Order> orders = new ArrayList<>();
        Optional<Order> optionalOrder = orderRepository.findById(key);
        optionalOrder.ifPresent(orders::add);
        return orders;
    }

    @Override
    public Page<OrderDTO> getCompanyAllOrders(Pageable pageable) {
        Page<OrderDTO> page = new PageImpl<>(new ArrayList<>(), pageable, 0);
        String login = SecurityUtils.getCurrentUserLogin().orElse("");
        Optional<User> userOptional = userRepository.findOneByLogin(login);
        if (userOptional.isPresent()) {
            Optional<List<Order>> optionalOrders = orderRepository.findByCompanyId(userOptional.get().getCompanyId());
            if (optionalOrders.isPresent()) {
                List<OrderDTO> orderDTOS = new ArrayList<>();

                Map<String, Object> res = get(optionalOrders.get().get(0).getCartId());
                for (Order order : optionalOrders.get()) {
                    OrderDTO dto = orderMapper.orderToOrderDTO(order);
                    dto.setBillingAddress((BillingAddress) res.get("billingAddress"));
                    dto.setDeliveryAddress((DeliveryAddress) res.get("deliveryAddress"));
                    orderDTOS.add(dto);
                }
                int start = (int) pageable.getOffset();
                int end = Math.min((start + pageable.getPageSize()), orderDTOS.size());
                page = new PageImpl<>(orderDTOS.subList(start, end), pageable, orderDTOS.size());
            }
        }
        return page;
    }

    /**
     * Get all the orders.
     *
     * @return the list of entities.
     */
    @Override
    public List<Order> findAll() {
        log.debug("Request to get all Orders");
        return orderRepository.findAll();
    }

    /**
     * Get one order by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<Order> findOne(String id) {
        log.debug("Request to get Order : {}", id);
        return orderRepository.findById(id);
    }

    /**
     * Delete the order by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Order : {}", id);
        orderRepository.deleteById(id);
    }
}
