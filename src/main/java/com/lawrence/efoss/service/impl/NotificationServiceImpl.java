package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.domain.User;
import com.lawrence.efoss.repository.UserRepository;
import com.lawrence.efoss.security.SecurityUtils;
import com.lawrence.efoss.service.NotificationService;
import com.lawrence.efoss.domain.EfossNotification;
import com.lawrence.efoss.domain.Order;
import com.lawrence.efoss.repository.NotificationRepository;
import com.lawrence.efoss.repository.OrderRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link EfossNotification}.
 */
@Service
public class NotificationServiceImpl implements NotificationService {

    private final Logger log = LoggerFactory.getLogger(NotificationServiceImpl.class);

    private final NotificationRepository notificationRepository;

    private final OrderRepository orderRepository;

    private final UserRepository userRepository;

    public NotificationServiceImpl(NotificationRepository notificationRepository, OrderRepository orderRepository, UserRepository userRepository) {
        this.notificationRepository = notificationRepository;
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a notification.
     *
     * @param efossNotification the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EfossNotification save(EfossNotification efossNotification) {
        log.debug("Request to save Notification : {}", efossNotification);
        ZonedDateTime now
             = ZonedDateTime.now();
        if (efossNotification.getCreatedAt() == null) {
            efossNotification.setCreatedAt(now);
        }
        efossNotification.setUpdatedAt(now);
        return notificationRepository.save(efossNotification);
    }

    /**
     * Get all the notifications.
     *
     * @return the list of entities.
     */
    @Override
    public List<EfossNotification> findAll() {
        log.debug("Request to get all Notifications");
        return notificationRepository.findAll();
    }

    /**
     * Get one notification by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<EfossNotification> findOne(String id) {
        log.debug("Request to get Notification : {}", id);
        return notificationRepository.findById(id);
    }

    /**
     * Delete the notification by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Notification : {}", id);
        notificationRepository.deleteById(id);
    }

    @Override
    public List<EfossNotification> get() {
        List<EfossNotification> efossNotifications = new ArrayList<>();
        String login = SecurityUtils.getCurrentUserLogin().orElse("");
        log.debug("login {} ", login);
        Optional<User> optionalUser = userRepository.findOneByLogin(login);
        if (optionalUser.isPresent()) {
            Optional<List<EfossNotification>> notificationList = notificationRepository.findByUserIdAndStatusOrderByCreatedAtDesc(optionalUser.get().getId(), "0");
            if (notificationList.isPresent()) {
                efossNotifications = notificationList.get();
            }
        }
        return efossNotifications;
    }

    @Scheduled(fixedDelay = 1000 * 10)
    public void send() {
        Optional<List<Order>> orders = orderRepository.findByStatus("9");
        if (orders.isPresent()) {
            for (Order order : orders.get()) {
                // log.debug
            }
        }
    }
}
