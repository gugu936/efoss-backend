package com.lawrence.efoss.service.impl;

import com.hazelcast.util.StringUtil;
import com.lawrence.efoss.domain.Product;
import com.lawrence.efoss.repository.ProductRepository;
import com.lawrence.efoss.service.ProductService;
import com.lawrence.efoss.service.dto.ProductDTO;
import com.lawrence.efoss.service.mapper.ProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Product}.
 */
@Service
public class ProductServiceImpl implements ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    private final ProductRepository productRepository;

    private final CompanyServiceImpl companyService;

    private final ProductMapper mapper;

    private final MongoTemplate template;

    public ProductServiceImpl(ProductRepository productRepository, CompanyServiceImpl companyService, ProductMapper mapper, MongoTemplate template) {
        this.productRepository = productRepository;
        this.companyService = companyService;
        this.mapper = mapper;
        this.template = template;
    }

    /**
     * Save a product.
     *
     * @param product the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Product save(Product product) {
        log.debug("Request to save Product : {}", product);
        ZonedDateTime now = ZonedDateTime.now();
        if (product.getCreatedAt() == null) {
            product.setCreatedAt(now);
        }
        product.setUpdatedAt(now);
        return productRepository.save(product);
    }

    /**
     * Get all the products.
     *
     * @return the list of entities.
     */
    @Override
    public List<Product> findAll() {
        log.debug("Request to get all Products");
        return productRepository.findAll();
    }

    @Override
    public Page<ProductDTO> findAllByCompanyId(Pageable pageable) {
        Page<ProductDTO> page = new PageImpl<>(new ArrayList<>(), pageable, 0);
        String companyId = companyService.getCompanyIdByUsername();
        if (!StringUtil.isNullOrEmptyAfterTrim(companyId)) {
            log.debug("companyId {}", companyId);
            Page<Product> productPage = productRepository.findByCompanyId(pageable, companyId);
            page = getProductDTOS(pageable, productPage);
        }
        return page;
    }

    private Page<ProductDTO> getProductDTOS(Pageable pageable, Page<Product> productPage) {
        Page<ProductDTO> page;
        List<Product> productList = productPage.getContent();
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), productList.size());
        page = new PageImpl<>(mapper.productsToProductDTOs(productList.subList(start, end)), pageable, productList.size());
        return page;
    }

    @Override
    public Page<ProductDTO> getProductsByCategory(String id, Pageable pageable) {
        Page<ProductDTO> page = new PageImpl<>(new ArrayList<>(), pageable, 0);
        Page<Product> productPage = productRepository.findByCategoryIdAndActive(pageable, id, true);
        page = getProductDTOS(pageable, productPage);
        return page;
    }

    @Override
    public Page<ProductDTO> customerSearch(String key, Pageable pageable) {
        List<Product> products = search("active", true, key);
        return getProductDTOS(pageable, products);
    }

    @Override
    public Page<ProductDTO> companySearch(String key, Pageable pageable) {
        String companyId = companyService.getCompanyIdByUsername();
        List<Product> products = search("company_id", companyId, key);
        return getProductDTOS(pageable, products);
    }

    private Page<ProductDTO> getProductDTOS(Pageable pageable, List<Product> products) {
        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), products.size());
        Page<ProductDTO> page = new PageImpl<>(mapper.productsToProductDTOs(products.subList(start, end)), pageable, products.size());
        return page;
    }

    private List<Product> search(String field, Object value, String key) {
        log.debug("search {}", key);
//        String regex = MongoRegexCreator.INSTANCE
//            .toRegularExpression(key, MongoRegexCreator.MatchMode.CONTAINING);
        Query query = new Query();
        query.addCriteria(Criteria.where(field).is(value));
        Criteria criteria = new Criteria();
        criteria.orOperator(Criteria.where("name").regex(".*"+key+".*", "i"),
            Criteria.where("brand").regex(".*"+key+".*", "i"),
            Criteria.where("description").regex(".*"+key+".*", "i"));
        query.addCriteria(criteria);
        List<Product> products = template.find(query, Product.class);
        log.debug("products size {}", products.size());
        return products;
    }

    /**
     * Get one product by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<Product> findOne(String id) {
        log.debug("Request to get Product : {}", id);
        return productRepository.findById(id);
    }

    /**
     * Delete the product by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Product : {}", id);
        productRepository.deleteById(id);
    }
}
