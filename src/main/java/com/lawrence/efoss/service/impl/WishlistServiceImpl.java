package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.domain.User;
import com.lawrence.efoss.repository.UserRepository;
import com.lawrence.efoss.security.SecurityUtils;
import com.lawrence.efoss.service.WishlistService;
import com.lawrence.efoss.domain.Wishlist;
import com.lawrence.efoss.repository.WishlistRepository;
import com.lawrence.efoss.service.dto.WishlistDTO;
import com.lawrence.efoss.service.mapper.WishlistMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Wishlist}.
 */
@Service
public class WishlistServiceImpl implements WishlistService {

    private final Logger log = LoggerFactory.getLogger(WishlistServiceImpl.class);

    private final WishlistRepository wishlistRepository;

    private final UserRepository userRepository;

    private final WishlistMapper wishlistMapper;

    public WishlistServiceImpl(WishlistRepository wishlistRepository, UserRepository userRepository, WishlistMapper wishlistMapper) {
        this.wishlistRepository = wishlistRepository;
        this.userRepository = userRepository;
        this.wishlistMapper = wishlistMapper;
    }

    /**
     * Save a wishlist.
     *
     * @param wishlist the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Wishlist save(Wishlist wishlist) {
        log.debug("Request to save Wishlist : {}", wishlist);
        ZonedDateTime now  = ZonedDateTime.now();
        if (wishlist.getCreatedAt() == null) {
            wishlist.setCreatedAt(now);
        }
        wishlist.setUpdatedAt(now);
        return wishlistRepository.save(wishlist);
    }

    @Override
    public WishlistDTO getCustomerWishlist() {
        WishlistDTO dto = new WishlistDTO();
        String login = SecurityUtils.getCurrentUserLogin().orElse("");
        if (StringUtils.isNotBlank(login)) {
            Optional<User> user = userRepository.findOneByLogin(login);
            if (user.isPresent()) {
                Optional<Wishlist> optionalWishlist = wishlistRepository.getByUserId(user.get().getId());
                if (optionalWishlist.isPresent()) {
                    dto = wishlistMapper.wishlistToWishlistDTO(optionalWishlist.get());
                } else {
                    Wishlist wishlist = new Wishlist();
                    wishlist.setUserId(user.get().getId());
                    wishlist = save(wishlist);
                    dto = wishlistMapper.wishlistToWishlistDTO(wishlist);
                }
            }
        }
        return dto;
    }

    /**
     * Get all the wishlists.
     *
     * @return the list of entities.
     */
    @Override
    public List<Wishlist> findAll() {
        log.debug("Request to get all Wishlists");
        return wishlistRepository.findAll();
    }

    /**
     * Get one wishlist by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<Wishlist> findOne(String id) {
        log.debug("Request to get Wishlist : {}", id);
        return wishlistRepository.findById(id);
    }

    /**
     * Delete the wishlist by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Wishlist : {}", id);
        wishlistRepository.deleteById(id);
    }
}
