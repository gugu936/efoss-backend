package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.domain.BillingAddress;
import com.lawrence.efoss.domain.User;
import com.lawrence.efoss.repository.BillingAddressRepository;
import com.lawrence.efoss.repository.UserRepository;
import com.lawrence.efoss.security.SecurityUtils;
import com.lawrence.efoss.service.BillingAddressService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link BillingAddress}.
 */
@Service
public class BillingAddressServiceImpl implements BillingAddressService {

    private final Logger log = LoggerFactory.getLogger(BillingAddressServiceImpl.class);

    private final BillingAddressRepository billingAddressRepository;

    private final UserRepository userRepository;

    public BillingAddressServiceImpl(BillingAddressRepository billingAddressRepository, UserRepository userRepository) {
        this.billingAddressRepository = billingAddressRepository;
        this.userRepository = userRepository;
    }

    /**
     * Save a billingAddress.
     *
     * @param billingAddress the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BillingAddress save(BillingAddress billingAddress) {
        log.debug("Request to save BillingAddress : {}", billingAddress);
        return billingAddressRepository.save(billingAddress);
    }

    /**
     * Get all the billingAddresses.
     *
     * @return the list of entities.
     */
    @Override
    public List<BillingAddress> findAll() {
        log.debug("Request to get all BillingAddresses");
        return billingAddressRepository.findAll();
    }

    /**
     * Get one billingAddress by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<BillingAddress> findOne(String id) {
        log.debug("Request to get BillingAddress : {}", id);
        return billingAddressRepository.findById(id);
    }

    /**
     * Delete the billingAddress by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete BillingAddress : {}", id);
        billingAddressRepository.deleteById(id);
    }

    @Override
    public Optional<BillingAddress> getCustomerBillingAddress() {
        String login = SecurityUtils.getCurrentUserLogin().orElse("");
        if (StringUtils.isNotBlank(login)) {
            Optional<User> optionalUser = userRepository.findOneByLogin(login);
            if (optionalUser.isPresent()) {
                Optional<BillingAddress> bill = billingAddressRepository.findOneByUserId(optionalUser.get().getId());
                return bill;
            } else {
                BillingAddress billingAddress = new BillingAddress();
                ZonedDateTime now = ZonedDateTime.now();
                billingAddress.setCreatedAt(now);
                billingAddress.setUpdatedAt(now);
                billingAddressRepository.save(billingAddress);
                return Optional.of(billingAddress);
            }
        }
      return Optional.empty();
    }
}
