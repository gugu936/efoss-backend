package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.domain.Cart;
import com.lawrence.efoss.domain.User;
import com.lawrence.efoss.repository.CartRepository;
import com.lawrence.efoss.repository.UserRepository;
import com.lawrence.efoss.security.SecurityUtils;
import com.lawrence.efoss.service.CartService;
import com.lawrence.efoss.service.dto.CartDTO;
import com.lawrence.efoss.service.mapper.CartMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Cart}.
 */
@Service
public class CartServiceImpl implements CartService {

    private final Logger log = LoggerFactory.getLogger(CartServiceImpl.class);

    private final CartRepository cartRepository;

    private final CartMapper cartMapper;

    private final UserRepository userRepository;

    private final MongoTemplate template;

    public CartServiceImpl(CartRepository cartRepository, CartMapper cartMapper, UserRepository userRepository, MongoTemplate template) {
        this.cartRepository = cartRepository;
        this.cartMapper = cartMapper;
        this.userRepository = userRepository;
        this.template = template;
    }

    /**
     * Save a cart.
     *
     * @param cart the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Cart save(Cart cart) {
        log.debug("Request to save Cart : {}", cart);
        return cartRepository.save(cart);
    }

    /**
     * Get all the carts.
     *
     * @return the list of entities.
     */
    @Override
    public List<Cart> findAll() {
        log.debug("Request to get all Carts");
        return cartRepository.findAll();
    }

    /**
     * Get one cart by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<Cart> findOne(String id) {
        log.debug("Request to get Cart : {}", id);
        return cartRepository.findById(id);
    }


    @Override
    public CartDTO getUserCart() {
        CartDTO cartDTO = new CartDTO();
        String login = SecurityUtils.getCurrentUserLogin().orElse("");
        if (StringUtils.isNotBlank(login)) {
            Optional<User> userOptional = userRepository.findOneByLogin(login);
            if (userOptional.isPresent()) {
                Query query = new Query();
                query.addCriteria(Criteria.where("user_id").is(userOptional.get().getId()).and("status").ne("closed"));
                Cart cart = template.findOne(query, Cart.class);
                if (cart == null) {
                    cart = new Cart();
                    cart.setUserId(userOptional.get().getId());
                    cart.setStatus("open");
                    ZonedDateTime now = ZonedDateTime.now();
                    cart.setCreatedAt(now);
                    cart.setUpdatedAt(now);
                    cartRepository.save(cart);
                }
                cartDTO = cartMapper.cartToCartDTO(cart);
            }
        }
        return cartDTO;
    }

    /**
     * Delete the cart by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Cart : {}", id);
        cartRepository.deleteById(id);
    }
}
