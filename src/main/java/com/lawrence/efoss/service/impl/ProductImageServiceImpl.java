package com.lawrence.efoss.service.impl;

import com.lawrence.efoss.service.ProductImageService;
import com.lawrence.efoss.domain.ProductImage;
import com.lawrence.efoss.repository.ProductImageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ProductImage}.
 */
@Service
public class ProductImageServiceImpl implements ProductImageService {

    private final Logger log = LoggerFactory.getLogger(ProductImageServiceImpl.class);

    private final ProductImageRepository productImageRepository;

    public ProductImageServiceImpl(ProductImageRepository productImageRepository) {
        this.productImageRepository = productImageRepository;
    }

    /**
     * Save a productImage.
     *
     * @param productImage the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProductImage save(ProductImage productImage) {
        log.debug("Request to save ProductImage : {}", productImage);
        return productImageRepository.save(productImage);
    }

    @Override
    public byte[] getImageByProductId(String id, String seq) {
        Optional<ProductImage> optionalProductImage = productImageRepository.findByProductIdAndSequence(id, Integer.valueOf(seq));
        return optionalProductImage.map(ProductImage::getImage).orElse(null);
    }

    /**
     * Get all the productImages.
     *
     * @return the list of entities.
     */
    @Override
    public List<ProductImage> findAll() {
        log.debug("Request to get all ProductImages");
        return productImageRepository.findAll();
    }

    @Override
    public byte[] getImageByProductId(String id) {
        Optional<ProductImage> optionalProductImage = productImageRepository.findByProductIdAndSequence(id, 0);
        return optionalProductImage.map(ProductImage::getImage).orElse(null);
    }

    /**
     * Get one productImage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    public Optional<ProductImage> findOne(String id) {
        log.debug("Request to get ProductImage : {}", id);
        return productImageRepository.findById(id);
    }

    /**
     * Delete the productImage by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete ProductImage : {}", id);
        productImageRepository.deleteById(id);
    }
}
