package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.CartItem;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link CartItem}.
 */
public interface CartItemService {

    /**
     * Save a cartItem.
     *
     * @param cartItem the entity to save.
     * @return the persisted entity.
     */
    CartItem save(CartItem cartItem);

    /**
     * Get all the cartItems.
     *
     * @return the list of entities.
     */
    List<CartItem> findAll();

    /**
     * Get the "id" cartItem.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CartItem> findOne(String id);

    /**
     * Delete the "id" cartItem.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
