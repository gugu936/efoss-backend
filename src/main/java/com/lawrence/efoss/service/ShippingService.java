package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.Shipping;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Shipping}.
 */
public interface ShippingService {

    /**
     * Save a shipping.
     *
     * @param shipping the entity to save.
     * @return the persisted entity.
     */
    Shipping save(Shipping shipping);

    /**
     * Get all the shippings.
     *
     * @return the list of entities.
     */
    List<Shipping> findAll();

    /**
     * Get the "id" shipping.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Shipping> findOne(String id);

    /**
     * Delete the "id" shipping.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
