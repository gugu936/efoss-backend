package com.lawrence.efoss.service.listener;

import com.lawrence.efoss.domain.Cart;
import com.lawrence.efoss.domain.EfossNotification;
import com.lawrence.efoss.domain.Order;
import com.lawrence.efoss.domain.User;
import com.lawrence.efoss.repository.CartRepository;
import com.lawrence.efoss.repository.NotificationRepository;
import com.lawrence.efoss.repository.UserRepository;
import com.lawrence.efoss.service.dto.OrderDTO;
import com.lawrence.efoss.service.impl.NotificationServiceImpl;
import com.lawrence.efoss.service.mapper.OrderMapper;
import com.lawrence.efoss.web.rest.NotificationResource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderBeforeSaveListener extends AbstractMongoEventListener<Order> {

    private final Logger log = LoggerFactory.getLogger(OrderBeforeSaveListener.class);

    private final OrderMapper orderMapper;

    private final CartRepository cartRepository;

    private final NotificationRepository notificationRepository;

    private final NotificationServiceImpl notificationService;

    private final NotificationResource notificationResource;

    private final UserRepository userRepository;

    public OrderBeforeSaveListener(OrderMapper orderMapper, CartRepository cartRepository, NotificationRepository notificationRepository, NotificationServiceImpl notificationService, NotificationResource notificationResource, UserRepository userRepository) {
        this.orderMapper = orderMapper;
        this.cartRepository = cartRepository;
        this.notificationRepository = notificationRepository;
        this.notificationService = notificationService;
        this.notificationResource = notificationResource;
        this.userRepository = userRepository;
    }

    @Override
    public void onBeforeSave(BeforeSaveEvent<Order> event) {
        log.debug("onBeforeSave");
        if (event.getDocument() != null && event.getSource().getStatus().equals(event.getDocument().getString("status"))) {
            OrderDTO orderDTO = orderMapper.orderToOrderDTO(event.getSource());
            Optional<Cart> optionalCart = cartRepository.findById(event.getSource().getCartId());
            EfossNotification efossNotification = new EfossNotification();
            efossNotification.setStatus("0");
            efossNotification.setType("order");
//            optionalCart.ifPresent(cart -> efossNotification.setUserId(cart.getUserId()));
            if (optionalCart.isPresent()) {
                String userId =  optionalCart.get().getUserId();
                efossNotification.setUserId(userId);
                Optional<User> optionalUser = userRepository.findById(userId);
                if (StringUtils.equals(event.getDocument().get("status").toString(), "3")) {
                    try {
                        if (optionalUser.isPresent()) {
                            log.info("sending notification");
                            notificationResource.send("Order Status Updated", "your products is shipped", optionalUser.get().getToken());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    efossNotification.setMessage("your products is shipped");
                }

                if (StringUtils.equals(event.getDocument().get("status").toString(), "4")) {
                    try {
                        if (optionalUser.isPresent()) {
                            log.info("sending notification");
                            notificationResource.send("Order Status Updated", "your order has arrival", optionalUser.get().getToken());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    efossNotification.setMessage("your order " + event.getDocument().getObjectId("_id").toHexString() + " has arrival, please go to " + orderDTO.getCompany().getAddress() + " pick up ");

                }
                if (StringUtils.equals(event.getDocument().get("status").toString(), "4") || StringUtils.equals(event.getDocument().get("status").toString(), "3")) {
                    notificationService.save(efossNotification);
                }
            }
        }
    }
}
