package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.CustomerOrder;
import com.lawrence.efoss.service.dto.CustomerOrderDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link CustomerOrder}.
 */
public interface CustomerOrderService {

    /**
     * Save a customerOrder.
     *
     * @param customerOrder the entity to save.
     * @return the persisted entity.
     */
    CustomerOrder save(CustomerOrder customerOrder);

    /**
     * Get all the customerOrders.
     *
     * @return the list of entities.
     */
    List<CustomerOrder> findAll();

    /**
     * Get the "id" customerOrder.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CustomerOrder> findOne(String id);

    List<CustomerOrderDTO> getUserAllOrders();

    CustomerOrderDTO getOrderByCartId(String cartId);

    /**
     * Delete the "id" customerOrder.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
