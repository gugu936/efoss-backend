package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.Order;
import com.lawrence.efoss.service.dto.OrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Order}.
 */
public interface OrderService {

    /**
     * Save a order.
     *
     * @param order the entity to save.
     * @return the persisted entity.
     */
    Order save(Order order);

    /**
     * Get all the orders.
     *
     * @return the list of entities.
     */
    List<Order> findAll();

    Page<OrderDTO> searchOrders(Pageable pageable, String keyword);

    /**
     * Get the "id" order.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Order> findOne(String id);

    Page<OrderDTO> getCompanyAllOrders(Pageable pageable);

    /**
     * Delete the "id" order.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
