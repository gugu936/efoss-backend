package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.ShippingMethod;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ShippingMethod}.
 */
public interface ShippingMethodService {

    /**
     * Save a shippingMethod.
     *
     * @param shippingMethod the entity to save.
     * @return the persisted entity.
     */
    ShippingMethod save(ShippingMethod shippingMethod);

    /**
     * Get all the shippingMethods.
     *
     * @return the list of entities.
     */
    List<ShippingMethod> findAll();

    /**
     * Get the "id" shippingMethod.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShippingMethod> findOne(String id);

    /**
     * Delete the "id" shippingMethod.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
