package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.EfossNotification;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link EfossNotification}.
 */
public interface NotificationService {

    /**
     * Save a notification.
     *
     * @param efossNotification the entity to save.
     * @return the persisted entity.
     */
    EfossNotification save(EfossNotification efossNotification);

    /**
     * Get all the notifications.
     *
     * @return the list of entities.
     */
    List<EfossNotification> findAll();

    /**
     * Get the "id" notification.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EfossNotification> findOne(String id);

    List<EfossNotification> get ();

    /**
     * Delete the "id" notification.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
