package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.ProductImage;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ProductImage}.
 */
public interface ProductImageService {

    /**
     * Save a productImage.
     *
     * @param productImage the entity to save.
     * @return the persisted entity.
     */
    ProductImage save(ProductImage productImage);

    /**
     * Get all the productImages.
     *
     * @return the list of entities.
     */
    List<ProductImage> findAll();

    byte[] getImageByProductId(String id);

    byte[] getImageByProductId(String id,String seq);

    /**
     * Get the "id" productImage.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProductImage> findOne(String id);

    /**
     * Delete the "id" productImage.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
