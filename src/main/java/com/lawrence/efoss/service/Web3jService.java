package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.User;
import com.lawrence.efoss.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.web3j.crypto.*;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthEstimateGas;
import org.web3j.protocol.core.methods.response.EthGasPrice;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import static java.net.URLDecoder.decode;

@Service
public class Web3jService {

    // geth --datadir="ethdata" --networkid 15 --nodiscover console --unlock 0x1A451a5F416c7F1d6E182cb001D2AD895110d70B --rpc --rpcaddr "0.0.0.0" --rpccorsdomain "*" --rpcapi "eth,net,web3,miner,debug,personal,rpc" --allow-insecure-unlock
    private final Environment environment;

    private Web3j web3j = Web3j.build(new HttpService());

    private final UserRepository userRepository;

    private final Logger log = LoggerFactory.getLogger(Web3jService.class);

    public Web3jService(Environment environment, UserRepository userRepository) {
        this.environment = environment;
        //   this.userRepository = userRepository;
        this.userRepository = userRepository;
    }

    private final String wallet = "/home/lawrence/Documents/Projects/CityU/efoss/backend/ethdata/keystore/";

    // private final UserRepository userRepository;

    public BigDecimal getBalance(String account) {
        try {
            EthGetBalance ethGetBalance = web3j
                .ethGetBalance(account, DefaultBlockParameterName.LATEST)
                .sendAsync()
                .get();

            BigInteger wei = ethGetBalance.getBalance();
            BigDecimal ether = Convert.fromWei(wei.toString(), Convert.Unit.ETHER);
            log.debug("wei {} ether {}", wei, ether);
            return ether;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return BigDecimal.ZERO;
    }

    @Async
    public void createAccount(User user) {

        try {
            ECKeyPair keyPair = Keys.createEcKeyPair();
            WalletFile wallet = Wallet.createStandard(user.getPassword(), keyPair);

            log.debug("Priate key: " + keyPair.getPrivateKey().toString(16));
            log.debug("Account: " + wallet.getAddress());

            String fileName = WalletUtils.generateNewWalletFile(
                user.getPassword(),
                new File(resolvePathPrefix() + this.wallet));

            user.setAccount(fileName.split("--")[2].replace(".json", ""));
            user.setWallet(fileName);
            userRepository.save(user);
        } catch (Exception e) {
            log.error("Error: " + e.getMessage());
        }
    }

    public BigDecimal fee(String account) {
        try {
            String efossAccount = environment.getProperty("efoss.e-account");
            EthGasPrice price = web3j.ethGasPrice().send();
            EthEstimateGas ethEstimateGas = web3j.ethEstimateGas(
                Transaction.createEthCallTransaction(
                    "0x" + account, efossAccount,
                    "0x10"))
                .send();
            BigInteger gasLimit = ethEstimateGas.getAmountUsed();
            return Convert.fromWei(String.valueOf(price.getGasPrice().doubleValue() * gasLimit.doubleValue()), Convert.Unit.ETHER);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BigDecimal.ZERO;
    }

    @Async
    public void send(Credentials credentials, String to, double value) {
        try {

            TransactionReceipt transactionReceipt = Transfer.sendFunds(
                web3j, credentials, to,
                BigDecimal.valueOf(value), Convert.Unit.ETHER)
                .send();
            log.debug(" value {}", value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void decrease(User user, double value) {
        String efossAccount = environment.getProperty("efoss.e-account");
        try {
            Credentials credentials = WalletUtils.loadCredentials(user.getPassword(), resolvePathPrefix() + wallet + user.getWallet());
            send(credentials, efossAccount, value);
            log.debug("account {} wallet {} value {} balance {}", user.getAccount(), user.getWallet(), value, getBalance("0x" + user.getAccount()));
        } catch (IOException | CipherException e) {
            e.printStackTrace();
        }
    }

    public void increase(User user, double value) {
        String password = Objects.requireNonNull(environment.getProperty("efoss.e-password"));
        String walletFile = Objects.requireNonNull(environment.getProperty("efoss.e-wallet"));
        try {
            Credentials credentials = WalletUtils.loadCredentials(password, resolvePathPrefix() + wallet + walletFile);
            send(credentials, "0x" + user.getAccount(), value);
        } catch (IOException | CipherException e) {
            e.printStackTrace();
        }
    }

    private String resolvePathPrefix() {
        String fullExecutablePath;
        try {
            fullExecutablePath = decode(this.getClass().getResource("").getPath(), StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            /* try without decoding if this ever happens */
            fullExecutablePath = this.getClass().getResource("").getPath();
        }
        String rootPath = Paths.get(".").toUri().normalize().getPath();
        String extractedPath = fullExecutablePath.replace(rootPath, "");
        int extractionEndIndex = extractedPath.indexOf("target/");
        if (extractionEndIndex <= 0) {
            return "";
        }
        return extractedPath.substring(0, extractionEndIndex);
    }
}
