package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.BillingAddress;

import java.util.List;
import java.util.Optional;

/**createdAt
 * Service Interface for managing {@link BillingAddress}.
 */
public interface BillingAddressService {

    /**
     * Save a billingAddress.
     *
     * @param billingAddress the entity to save.
     * @return the persisted entity.
     */
    BillingAddress save(BillingAddress billingAddress);

    /**
     * Get all the billingAddresses.
     *
     * @return the list of entities.
     */
    List<BillingAddress> findAll();

    /**
     * Get the "id" billingAddress.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BillingAddress> findOne(String id);

    Optional<BillingAddress> getCustomerBillingAddress();

    /**
     * Delete the "id" billingAddress.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
