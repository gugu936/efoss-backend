package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.Product;
import com.lawrence.efoss.service.dto.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Product}.
 */
public interface ProductService {

    /**
     * Save a product.
     *
     * @param product the entity to save.
     * @return the persisted entity.
     */
    Product save(Product product);

    /**
     * Get all the products.
     *
     * @return the list of entities.
     */
    List<Product> findAll();

    Page<ProductDTO> findAllByCompanyId(Pageable pageable);

    Page<ProductDTO> getProductsByCategory(String id, Pageable pageable);

    Page<ProductDTO> customerSearch(String key, Pageable pageable);

    Page<ProductDTO> companySearch(String key, Pageable pageable);
    /**
     * Get the "id" product.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Product> findOne(String id);

    /**
     * Delete the "id" product.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
