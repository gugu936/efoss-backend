package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.Coin;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Coin}.
 */
public interface CoinService {

    /**
     * Save a coin.
     *
     * @param coin the entity to save.
     * @return the persisted entity.
     */
    Coin save(Coin coin);

    /**
     * Get all the coins.
     *
     * @return the list of entities.
     */
    List<Coin> findAll();

    /**
     * Get the "id" coin.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Coin> findOne(String id);

    /**
     * Delete the "id" coin.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
