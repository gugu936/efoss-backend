package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.Wishlist;
import com.lawrence.efoss.service.dto.WishlistDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Wishlist}.
 */
public interface WishlistService {

    /**
     * Save a wishlist.
     *
     * @param wishlist the entity to save.
     * @return the persisted entity.
     */
    Wishlist save(Wishlist wishlist);

    /**
     * Get all the wishlists.
     *
     * @return the list of entities.
     */
    List<Wishlist> findAll();

    /**
     * Get the "id" wishlist.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Wishlist> findOne(String id);

    WishlistDTO getCustomerWishlist();

    /**
     * Delete the "id" wishlist.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
