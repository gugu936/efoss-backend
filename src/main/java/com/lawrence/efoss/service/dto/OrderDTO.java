package com.lawrence.efoss.service.dto;

import com.lawrence.efoss.domain.*;

import java.math.BigDecimal;
import java.util.List;

public class OrderDTO {
    private String id;

    private String status;

    private PaymentMethod paymentMethod;

    private ShippingMethod shippingMethod;

    private BigDecimal total;

    private List<CartItemDTO> cartItems;

    private BillingAddress billingAddress;

    private DeliveryAddress deliveryAddress;

    private Shipping shipping;

    private String cartId;

    private Company company;

    private String createdAt;

    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getStatus() {
        return status;
    }

    public List<CartItemDTO> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItemDTO> cartItems) {
        this.cartItems = cartItems;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public DeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public ShippingMethod getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public OrderDTO() {
    }

    public OrderDTO(OrderDTO orderDTO) {
        this.billingAddress = orderDTO.getBillingAddress();
        this.deliveryAddress = orderDTO.getDeliveryAddress();
        this.id = orderDTO.id;
        this.cartId = orderDTO.cartId;
        this.shipping = orderDTO.getShipping();
        this.shippingMethod = orderDTO.getShippingMethod();
        this.status = orderDTO.status;
        this.cartItems = orderDTO.cartItems;
        this.paymentMethod = orderDTO.paymentMethod;
        this.total = orderDTO.total;
        this.company = orderDTO.getCompany();
        this.createdAt = orderDTO.createdAt;
        this.updatedAt = orderDTO.updatedAt;
    }
}
