package com.lawrence.efoss.service.dto;

import java.util.List;

public class CartDTO {
    private String id;

    private String userId;

    private String status;

    private List<CartItemDTO> cartItems;

    private String createdAt;

    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<CartItemDTO> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItemDTO> cartItems) {
        this.cartItems = cartItems;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public CartDTO(){}

    public CartDTO(CartDTO cartDTO) {
        this.id = cartDTO.id;
        this.userId = cartDTO.userId;
        this.status = cartDTO.status;
        this.cartItems = cartDTO.cartItems;
        this.createdAt = cartDTO.createdAt;
        this.updatedAt = cartDTO.updatedAt;
    }
}
