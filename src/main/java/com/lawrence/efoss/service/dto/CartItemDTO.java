package com.lawrence.efoss.service.dto;

import java.math.BigDecimal;

public class CartItemDTO {
    private String id;

    private String cartId;

    private ProductDTO product;

    private Integer qty;

    private BigDecimal price;

    private String color;

    private String size;

    private String createdAt;

    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public CartItemDTO() {}

    public CartItemDTO(CartItemDTO cartItemDTO) {
        this.id = cartItemDTO.id;
        this.cartId = cartItemDTO.cartId;
        this.product = cartItemDTO.product;
        this.color = cartItemDTO.getColor();
        this.size = cartItemDTO.getSize();
        this.qty = cartItemDTO.qty;
        this.price = cartItemDTO.price;
        this.createdAt = cartItemDTO.createdAt;
        this.updatedAt = cartItemDTO.updatedAt;
    }

}
