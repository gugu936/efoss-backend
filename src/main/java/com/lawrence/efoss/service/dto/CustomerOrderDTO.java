package com.lawrence.efoss.service.dto;

import com.lawrence.efoss.domain.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class CustomerOrderDTO {
    private String id;

    private String cartId;

    private String status;

    private Map<Company, List<OrderDTO>> orders;

    private PaymentMethod paymentMethod;

    private ShippingMethod shippingMethod;

    private UserDTO user;

    private Boolean sameAsBilling;

    private BillingAddress billingAddress;

    private DeliveryAddress deliveryAddress;

    private BigDecimal discount;

    private BigDecimal total;

    private String createdAt;

    private String updatedAt;

    private Boolean useEWallet;

    public Boolean getUseEWallet() {
        return useEWallet;
    }

    public void setUseEWallet(Boolean useEWallet) {
        this.useEWallet = useEWallet;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<Company, List<OrderDTO>> getOrders() {
        return orders;
    }

    public void setOrders(Map<Company, List<OrderDTO>> orders) {
        this.orders = orders;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getSameAsBilling() {
        return sameAsBilling;
    }

    public void setSameAsBilling(Boolean sameAsBilling) {
        this.sameAsBilling = sameAsBilling;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public DeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(DeliveryAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public ShippingMethod getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(ShippingMethod shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public CustomerOrderDTO() {}

    public CustomerOrderDTO(CustomerOrderDTO customerOrderDTO) {
        this.id = customerOrderDTO.id;
        this.cartId = customerOrderDTO.cartId;
        this.status = customerOrderDTO.status;
        this.useEWallet = customerOrderDTO.useEWallet;
        this.orders = customerOrderDTO.orders;
        this.paymentMethod = customerOrderDTO.paymentMethod;
        this.shippingMethod = customerOrderDTO.shippingMethod;
        this.discount = customerOrderDTO.discount;
        this.sameAsBilling = customerOrderDTO.sameAsBilling;
        this.billingAddress = customerOrderDTO.billingAddress;
        this.deliveryAddress = customerOrderDTO.deliveryAddress;
        this.total = customerOrderDTO.total;
        this.user = customerOrderDTO.user;
        this.createdAt = customerOrderDTO.createdAt;
        this.updatedAt = customerOrderDTO.updatedAt;
    }
    //    public CustomerOrderDTO(CustomerOrderDTO customerOrderDTO) {
//        this.id = customerOrderDTO.getId();
//    }
}
