package com.lawrence.efoss.service.dto;

import com.lawrence.efoss.domain.Category;
import com.lawrence.efoss.domain.Company;

import java.math.BigDecimal;
import java.util.List;

public class ProductDTO {
    private String id;

    private String name;

    private Integer qty;

    private String description;

    private Company company;

    private String brand;

    private Boolean active;

    private BigDecimal shippingFee;

    private Category category;

    private BigDecimal price;

    private BigDecimal depth;

    private BigDecimal height;

    private Boolean refurbished;

    private BigDecimal width;

    private List<String> size;

    private List<String> color;

    private String createdAt;

    private String updatedAt;

    private BigDecimal weight;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<String> getSize() {
        return size;
    }

    public void setSize(List<String> size) {
        this.size = size;
    }

    public List<String> getColor() {
        return color;
    }

    public void setColor(List<String> color) {
        this.color = color;
    }

    public BigDecimal getDepth() {
        return depth;
    }

    public void setDepth(BigDecimal depth) {
        this.depth = depth;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public BigDecimal getWidth() {
        return width;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    public Boolean getRefurbished() {
        return refurbished;
    }

    public void setRefurbished(Boolean refurbished) {
        this.refurbished = refurbished;
    }

    public ProductDTO() {};

    public ProductDTO(ProductDTO product) {
        this.width = product.getWidth();
        this.height = product.getHeight();
        this.depth = product.getDepth();
        this.id = product.getId();
        this.size = product.getSize();
        this.color = product.getColor();
        this.refurbished = product.getRefurbished();
        this.name = product.getName();
        this.qty = product.getQty();
        this.description = product.getDescription();
        this.company = product.getCompany();
        this.brand = product.getBrand();
        this.category = product.getCategory();
        this.price = product.getPrice();
        this.weight = product.getWeight();
        this.active = product.getActive();
        this.shippingFee = product.getShippingFee();
        this.createdAt = product.getCreatedAt();
        this.updatedAt = product.getUpdatedAt();
    }
}
