package com.lawrence.efoss.service.dto;

import java.util.List;

public class WishlistDTO {
    private String id;

    private String userId;

    private List<ProductDTO> products;

    private String createdAt;

    private String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public WishlistDTO () {};

    public WishlistDTO(WishlistDTO wishlistDTO) {
        this.id = wishlistDTO.id;
        this.userId = wishlistDTO.userId;
        this.products = wishlistDTO.products;
        this.createdAt = wishlistDTO.createdAt;
        this.updatedAt = wishlistDTO.updatedAt;
    }
}
