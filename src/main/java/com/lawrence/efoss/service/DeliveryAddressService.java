package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.DeliveryAddress;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link DeliveryAddress}.
 */
public interface DeliveryAddressService {

    /**
     * Save a deliveryAddress.
     *
     * @param deliveryAddress the entity to save.
     * @return the persisted entity.
     */
    DeliveryAddress save(DeliveryAddress deliveryAddress);

    /**
     * Get all the deliveryAddresses.
     *
     * @return the list of entities.
     */
    List<DeliveryAddress> findAll();

    /**
     * Get the "id" deliveryAddress.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DeliveryAddress> findOne(String id);

    Optional<DeliveryAddress> getCustomerDeliveryAddress();

    /**
     * Delete the "id" deliveryAddress.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
