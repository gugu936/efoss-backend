package com.lawrence.efoss.service;

import com.lawrence.efoss.domain.Cart;
import com.lawrence.efoss.service.dto.CartDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Cart}.
 */
public interface CartService {

    /**
     * Save a cart.
     *
     * @param cart the entity to save.
     * @return the persisted entity.
     */
    Cart save(Cart cart);

    /**
     * Get all the carts.
     *
     * @return the list of entities.
     */
    List<Cart> findAll();

    /**
     * Get the "id" cart.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Cart> findOne(String id);

    CartDTO getUserCart();

    /**
     * Delete the "id" cart.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
