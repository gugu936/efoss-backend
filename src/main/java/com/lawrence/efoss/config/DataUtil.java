package com.lawrence.efoss.config;

import com.lawrence.efoss.domain.BillingAddress;
import com.lawrence.efoss.domain.DeliveryAddress;
import com.lawrence.efoss.domain.User;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class DataUtil {

    private final Logger log = LoggerFactory.getLogger(DataUtil.class);

    public static String getDate(String str) {
        if (StringUtils.isNotBlank(str)) {
            return str.substring(0, 16).replace("T", " ");
            // 2020-04-25T00:00+08:00
        }
        return "";
    }

    public static DeliveryAddress deliveryAddress(BillingAddress billingAddress) {
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        deliveryAddress.setFirstName(billingAddress.getFirstName());
        deliveryAddress.setLastName(billingAddress.getLastName());
        deliveryAddress.setPhone(billingAddress.getPhone());
        deliveryAddress.setCity(billingAddress.getCity());
        deliveryAddress.setStreet(billingAddress.getStreet());
        deliveryAddress.setCreatedAt(billingAddress.getCreatedAt());
        deliveryAddress.setUpdatedAt(billingAddress.getUpdatedAt());
        return deliveryAddress;
    }

    public static int getDiscount(User user) {
        if (user.getePoint() == null) {
            user.setePoint(BigDecimal.ZERO);
        }
        double ePoint = user.getePoint().doubleValue();
        if (ePoint > 0 && ePoint <= 100) {
            return 1;
        } else if (ePoint > 100 && ePoint <= 200) {
            return 2;
        } else if (ePoint > 200 && ePoint <= 300) {
            return 3;
        } else if (ePoint > 300 && ePoint <= 400) {
            return 4;
        } else if (ePoint > 400 && ePoint <= 500) {
            return 5;
        } else if (ePoint > 500 && ePoint <= 600) {
            return 6;
        } else if (ePoint > 600 && ePoint <= 700) {
            return 7;
        } else if (ePoint > 700 && ePoint <= 800) {
            return 8;
        } else if (ePoint > 800 && ePoint <= 900) {
            return 9;
        } else if (ePoint > 900) {
            return 10;
        }
        return 0;
    }

}
