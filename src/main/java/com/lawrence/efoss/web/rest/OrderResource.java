package com.lawrence.efoss.web.rest;

import com.lawrence.efoss.config.DataUtil;
import com.lawrence.efoss.domain.CustomerOrder;
import com.lawrence.efoss.domain.Order;
import com.lawrence.efoss.repository.CustomerOrderRepository;
import com.lawrence.efoss.service.OrderService;
import com.lawrence.efoss.service.dto.CustomerOrderDTO;
import com.lawrence.efoss.service.dto.OrderDTO;
import com.lawrence.efoss.service.mapper.CustomerOrderMapper;
import com.lawrence.efoss.service.mapper.OrderMapper;
import com.lawrence.efoss.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.lawrence.efoss.domain.Order}.
 */
@RestController
@RequestMapping("/api")
public class OrderResource {

    private final Logger log = LoggerFactory.getLogger(OrderResource.class);

    private static final String ENTITY_NAME = "order";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderService orderService;

    private final OrderMapper orderMapper;

    private final CustomerOrderRepository customerOrderRepository;

    private final CustomerOrderMapper customerOrderMapper;

    public OrderResource(OrderService orderService, OrderMapper orderMapper, CustomerOrderRepository customerOrderRepository, CustomerOrderMapper customerOrderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
        this.customerOrderRepository = customerOrderRepository;
        this.customerOrderMapper = customerOrderMapper;
    }

    /**
     * {@code POST  /orders} : Create a new order.
     *
     * @param order the order to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new order, or with status {@code 400 (Bad Request)} if the order has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/orders")
    public ResponseEntity<Order> createOrder(@RequestBody Order order) throws URISyntaxException {
        log.debug("REST request to save Order : {}", order);
        if (order.getId() != null) {
            throw new BadRequestAlertException("A new order cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Order result = orderService.save(order);
        return ResponseEntity.created(new URI("/api/orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /orders} : Updates an existing order.
     *
     * @param order the order to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated order,
     * or with status {@code 400 (Bad Request)} if the order is not valid,
     * or with status {@code 500 (Internal Server Error)} if the order couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/orders")
    public ResponseEntity<Order> updateOrder(@RequestBody Order order) throws URISyntaxException {
        log.debug("REST request to update Order : {}", order);
        if (order.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Order result = orderService.save(order);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, order.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /orders} : get all the orders.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orders in body.
     */
    @GetMapping("/orders")
    public List<Order> getAllOrders() {
        log.debug("REST request to get all Orders");
        return orderService.findAll();
    }

    @GetMapping("/orders/company")
    public List<OrderDTO> getCompanyAllOrders(Pageable pageable) {
        log.debug("REST request to get all Orders");
        return orderService.getCompanyAllOrders(pageable).getContent();
    }

    @GetMapping("/orders/company/search/{keyword}")
    public ResponseEntity<List<OrderDTO>> searchOrders(Pageable pageable, @PathVariable String keyword) {
        log.debug("REST request to get all Orders");
        Page<OrderDTO> page = orderService.searchOrders(pageable, keyword);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * {@code GET  /orders/:id} : get the "id" order.
     *
     * @param id the id of the order to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the order, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/orders/{id}")
    public ResponseEntity<OrderDTO> getOrder(@PathVariable String id) {
        log.debug("REST request to get Order : {}", id);
        Optional<Order> optionalOrder = orderService.findOne(id);
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();
            OrderDTO orderDTO = orderMapper.orderToOrderDTO(order);
            Optional<CustomerOrder> optionalCustomerOrder = customerOrderRepository.findByCartId(order.getCartId());
            if (optionalCustomerOrder.isPresent()) {
                CustomerOrderDTO customerOrderDTO = customerOrderMapper.customerOrderToCustomerOrderDTO(optionalCustomerOrder.get());

                orderDTO.setBillingAddress(customerOrderDTO.getBillingAddress());
                if (customerOrderDTO.getSameAsBilling()) {
                    orderDTO.setDeliveryAddress(DataUtil.deliveryAddress(customerOrderDTO.getBillingAddress()));
                } else {
                    orderDTO.setDeliveryAddress(customerOrderDTO.getDeliveryAddress());
                }
            }
            return ResponseEntity.ok().body(orderDTO);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    /**
     * {@code DELETE  /orders/:id} : delete the "id" order.
     *
     * @param id the id of the order to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Void> deleteOrder(@PathVariable String id) {
        log.debug("REST request to delete Order : {}", id);
        orderService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
