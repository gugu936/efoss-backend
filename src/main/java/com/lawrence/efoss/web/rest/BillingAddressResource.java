package com.lawrence.efoss.web.rest;

import com.lawrence.efoss.domain.BillingAddress;
import com.lawrence.efoss.service.BillingAddressService;
import com.lawrence.efoss.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.lawrence.efoss.domain.BillingAddress}.
 */
@RestController
@RequestMapping("/api")
public class BillingAddressResource {

    private final Logger log = LoggerFactory.getLogger(BillingAddressResource.class);

    private static final String ENTITY_NAME = "billingAddress";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BillingAddressService billingAddressService;

    public BillingAddressResource(BillingAddressService billingAddressService) {
        this.billingAddressService = billingAddressService;
    }

    /**
     * {@code POST  /billing-addresses} : Create a new billingAddress.
     *
     * @param billingAddress the billingAddress to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new billingAddress, or with status {@code 400 (Bad Request)} if the billingAddress has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/billing-addresses")
    public ResponseEntity<BillingAddress> createBillingAddress(@RequestBody BillingAddress billingAddress) throws URISyntaxException {
        log.debug("REST request to save BillingAddress : {}", billingAddress);
        if (billingAddress.getId() != null) {
            throw new BadRequestAlertException("A new billingAddress cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BillingAddress result = billingAddressService.save(billingAddress);
        return ResponseEntity.created(new URI("/api/billing-addresses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /billing-addresses} : Updates an existing billingAddress.
     *
     * @param billingAddress the billingAddress to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated billingAddress,
     * or with status {@code 400 (Bad Request)} if the billingAddress is not valid,
     * or with status {@code 500 (Internal Server Error)} if the billingAddress couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/billing-addresses")
    public ResponseEntity<BillingAddress> updateBillingAddress(@RequestBody BillingAddress billingAddress) throws URISyntaxException {
        log.debug("REST request to update BillingAddress : {}", billingAddress);
        if (billingAddress.getId() == null) {
            return createBillingAddress(billingAddress);
        }
        BillingAddress result = billingAddressService.save(billingAddress);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, billingAddress.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /billing-addresses} : get all the billingAddresses.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of billingAddresses in body.
     */
    @GetMapping("/billing-addresses")
    public List<BillingAddress> getAllBillingAddresses() {
        log.debug("REST request to get all BillingAddresses");
        return billingAddressService.findAll();
    }

    /**
     * {@code GET  /billing-addresses/:id} : get the "id" billingAddress.
     *
     * @param id the id of the billingAddress to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the billingAddress, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/billing-addresses/{id}")
    public ResponseEntity<BillingAddress> getBillingAddress(@PathVariable String id) {
        log.debug("REST request to get BillingAddress : {}", id);
        Optional<BillingAddress> billingAddress = billingAddressService.findOne(id);
        return ResponseUtil.wrapOrNotFound(billingAddress);
    }

    @GetMapping("/billing-addresses/customer")
    public ResponseEntity<BillingAddress> getCustomerBillingAddress() {
        Optional<BillingAddress> billingAddress = billingAddressService.getCustomerBillingAddress();
        return ResponseUtil.wrapOrNotFound(billingAddress);
    }

    /**
     * {@code DELETE  /billing-addresses/:id} : delete the "id" billingAddress.
     *
     * @param id the id of the billingAddress to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/billing-addresses/{id}")
    public ResponseEntity<Void> deleteBillingAddress(@PathVariable String id) {
        log.debug("REST request to delete BillingAddress : {}", id);
        billingAddressService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
