package com.lawrence.efoss.web.rest;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.lawrence.efoss.domain.EfossNotification;
import com.lawrence.efoss.service.AndroidPushNotificationsService;
import com.lawrence.efoss.service.NotificationService;
import com.lawrence.efoss.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link EfossNotification}.
 */
@RestController
@RequestMapping("/api")
public class NotificationResource {

    private final Logger log = LoggerFactory.getLogger(NotificationResource.class);

    private static final String ENTITY_NAME = "notification";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NotificationService notificationService;

    private final AndroidPushNotificationsService androidPushNotificationsService;

    public NotificationResource(NotificationService notificationService, AndroidPushNotificationsService androidPushNotificationsService) {
        this.notificationService = notificationService;
        this.androidPushNotificationsService = androidPushNotificationsService;
    }

    /**
     * {@code POST  /notifications} : Create a new notification.
     *
     * @param efossNotification the notification to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new notification, or with status {@code 400 (Bad Request)} if the notification has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/notifications")
    public ResponseEntity<EfossNotification> createNotification(@RequestBody EfossNotification efossNotification) throws URISyntaxException {
        log.debug("REST request to save Notification : {}", efossNotification);
        if (efossNotification.getId() != null) {
            throw new BadRequestAlertException("A new notification cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EfossNotification result = notificationService.save(efossNotification);
        return ResponseEntity.created(new URI("/api/notifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /notifications} : Updates an existing notification.
     *
     * @param efossNotification the notification to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated notification,
     * or with status {@code 400 (Bad Request)} if the notification is not valid,
     * or with status {@code 500 (Internal Server Error)} if the notification couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/notifications")
    public ResponseEntity<EfossNotification> updateNotification(@RequestBody EfossNotification efossNotification) throws URISyntaxException {
        log.debug("REST request to update Notification : {}", efossNotification);
        if (efossNotification.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EfossNotification result = notificationService.save(efossNotification);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, efossNotification.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /notifications} : get all the notifications.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of notifications in body.
     */
    @GetMapping("/notifications")
    public List<EfossNotification> getAllNotifications() {
        log.debug("REST request to get all Notifications");
        return notificationService.findAll();
    }

    @RequestMapping(value = "/send", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> send(String title, String msg, String token) throws JSONException {

        Message message = Message.builder()
            .setToken(token)
            .setNotification(new Notification(title, msg))
            .putData("content", title)
            .putData("body", msg)
            .build();

        String response = "";
        try {
            response = FirebaseMessaging.getInstance().send(message);
            log.info("response {}", response);
        } catch (FirebaseMessagingException e) {
            log.error("Fail to send firebase notification", e);
        }
            return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/notifications/customer")
    public List<EfossNotification> getAllNotificationsByCustomer() {
        log.debug("REST request to get all Notifications");
        return notificationService.get();
    }

    /**
     * {@code GET  /notifications/:id} : get the "id" notification.
     *
     * @param id the id of the notification to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the notification, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/notifications/{id}")
    public ResponseEntity<EfossNotification> getNotification(@PathVariable String id) {
        log.debug("REST request to get Notification : {}", id);
        Optional<EfossNotification> notification = notificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(notification);
    }

    /**
     * {@code DELETE  /notifications/:id} : delete the "id" notification.
     *
     * @param id the id of the notification to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/notifications/{id}")
    public ResponseEntity<Void> deleteNotification(@PathVariable String id) {
        log.debug("REST request to delete Notification : {}", id);
        notificationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
