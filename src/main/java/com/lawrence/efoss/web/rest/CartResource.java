package com.lawrence.efoss.web.rest;

import com.lawrence.efoss.domain.Cart;
import com.lawrence.efoss.domain.User;
import com.lawrence.efoss.repository.CartRepository;
import com.lawrence.efoss.repository.UserRepository;
import com.lawrence.efoss.security.SecurityUtils;
import com.lawrence.efoss.service.CartService;
import com.lawrence.efoss.service.dto.CartDTO;
import com.lawrence.efoss.service.dto.CartItemDTO;
import com.lawrence.efoss.service.dto.ProductDTO;
import com.lawrence.efoss.service.mapper.CartMapper;
import com.lawrence.efoss.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.lawrence.efoss.config.DataUtil.getDiscount;

/**
 * REST controller for managing {@link com.lawrence.efoss.domain.Cart}.
 */
@RestController
@RequestMapping("/api")
public class CartResource {

    private final Logger log = LoggerFactory.getLogger(CartResource.class);

    private static final String ENTITY_NAME = "cart";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CartService cartService;

    private final CartRepository cartRepository;

    private final CartMapper cartMapper;

    private final UserRepository userRepository;

    public CartResource(CartService cartService, CartRepository cartRepository, CartMapper cartMapper, UserRepository userRepository) {
        this.cartService = cartService;
        this.cartRepository = cartRepository;
        this.cartMapper = cartMapper;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /carts} : Create a new cart.
     *
     * @param cart the cart to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cart, or with status {@code 400 (Bad Request)} if the cart has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/carts")
    public ResponseEntity<Cart> createCart(@RequestBody Cart cart) throws URISyntaxException {
        log.debug("REST request to save Cart : {}", cart);
        if (cart.getId() != null) {
            throw new BadRequestAlertException("A new cart cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Cart result = cartService.save(cart);
        return ResponseEntity.created(new URI("/api/carts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /carts} : Updates an existing cart.
     *
     * @param cart the cart to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cart,
     * or with status {@code 400 (Bad Request)} if the cart is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cart couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/carts")
    public ResponseEntity<Cart> updateCart(@RequestBody Cart cart) throws URISyntaxException {
        log.debug("REST request to update Cart : {}", cart);
        if (cart.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Cart result = cartService.save(cart);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cart.getId().toString()))
            .body(result);
    }


    @PostMapping("/carts/checkAvailability")
    public Map<String, Boolean> checkAvailability(){
        Map<String, Boolean> result = new HashMap<>();
        String login = SecurityUtils.getCurrentUserLogin().orElse("");
        if (StringUtils.isNotBlank(login)) {
            Optional<User> optionalUser = userRepository.findOneByLogin(login);
            if (optionalUser.isPresent()) {
                Optional<Cart> cart = cartRepository.findOneByUserIdAndStatus(optionalUser.get().getId(), "open");
                if (cart.isPresent()) {
                    CartDTO cartDTO = cartMapper.cartToCartDTO(cart.get());
                    boolean outOf = false;
                    for (CartItemDTO dto : cartDTO.getCartItems() ) {
                        if (dto.getProduct().getQty() < dto.getQty()) {
                            outOf = true;
                        }
                    }
                    result.put("result", outOf);
                }
            }
        }
        return result;
    }

    /**
     * {@code GET  /carts} : get all the carts.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of carts in body.
     */
    @GetMapping("/carts")
    public List<Cart> getAllCarts() {
        log.debug("REST request to get all Carts");
        return cartService.findAll();
    }

    @GetMapping("/carts/final-price")
    public Map<String, String> getFinalPrice() {
        Map<String, String> result = new HashMap<>();
        CartDTO cartDTO = getUserCart();
        double total = 0;
        for (CartItemDTO itemDTO : cartDTO.getCartItems()) {
            total += itemDTO.getPrice().doubleValue();
        }
        result.put("total", total + "");
        Optional<User> userOptional = userRepository.findById(cartDTO.getUserId());
        int discount = 0;
        if (userOptional.isPresent()) {
            discount = getDiscount(userOptional.get());
        }
        result.put("discount", discount + "");
        log.debug("getFinalPrice {} ", result);
        return result;
    }

    @GetMapping("/carts/shipping-fee")
    public Map<String, String> getShippingFee() {
        log.debug("getShippingFee");
        // get most expensive fee
        Map<String, String> result = new HashMap<>();
        Map<String, Double> map = new HashMap<>();
        CartDTO cartDTO = getUserCart();
        double total = 0;
        for (CartItemDTO itemDTO : cartDTO.getCartItems()) {
            ProductDTO productDTO = itemDTO.getProduct();
            String companyId = productDTO.getCompany().getId();
            if (map.get(companyId) == null) {
                map.put(companyId, productDTO.getShippingFee().doubleValue());
            } else {
                if (productDTO.getShippingFee().doubleValue() > map.get(companyId)) {
                    map.put(companyId, productDTO.getShippingFee().doubleValue());
                }
            }
        }
        for (Map.Entry<String, Double> entry : map.entrySet()) {
            total += entry.getValue();
        }
        result.put("fee", total + "");
        return result;
    }

    @GetMapping("/carts/getUserCart")
    public CartDTO getUserCart() {
        return cartService.getUserCart();
    }

    /**
     * {@code GET  /carts/:id} : get the "id" cart.
     *
     * @param id the id of the cart to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cart, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/carts/{id}")
    public ResponseEntity<Cart> getCart(@PathVariable String id) {
        log.debug("REST request to get Cart : {}", id);
        Optional<Cart> cart = cartService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cart);
    }

    /**
     * {@code DELETE  /carts/:id} : delete the "id" cart.
     *
     * @param id the id of the cart to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/carts/{id}")
    public ResponseEntity<Void> deleteCart(@PathVariable String id) {
        log.debug("REST request to delete Cart : {}", id);
        cartService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
