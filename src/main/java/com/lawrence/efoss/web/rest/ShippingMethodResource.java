package com.lawrence.efoss.web.rest;

import com.lawrence.efoss.domain.ShippingMethod;
import com.lawrence.efoss.service.ShippingMethodService;
import com.lawrence.efoss.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.lawrence.efoss.domain.ShippingMethod}.
 */
@RestController
@RequestMapping("/api")
public class ShippingMethodResource {

    private final Logger log = LoggerFactory.getLogger(ShippingMethodResource.class);

    private static final String ENTITY_NAME = "shippingMethod";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShippingMethodService shippingMethodService;

    public ShippingMethodResource(ShippingMethodService shippingMethodService) {
        this.shippingMethodService = shippingMethodService;
    }

    /**
     * {@code POST  /shipping-methods} : Create a new shippingMethod.
     *
     * @param shippingMethod the shippingMethod to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shippingMethod, or with status {@code 400 (Bad Request)} if the shippingMethod has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shipping-methods")
    public ResponseEntity<ShippingMethod> createShippingMethod(@RequestBody ShippingMethod shippingMethod) throws URISyntaxException {
        log.debug("REST request to save ShippingMethod : {}", shippingMethod);
        if (shippingMethod.getId() != null) {
            throw new BadRequestAlertException("A new shippingMethod cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShippingMethod result = shippingMethodService.save(shippingMethod);
        return ResponseEntity.created(new URI("/api/shipping-methods/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shipping-methods} : Updates an existing shippingMethod.
     *
     * @param shippingMethod the shippingMethod to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shippingMethod,
     * or with status {@code 400 (Bad Request)} if the shippingMethod is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shippingMethod couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shipping-methods")
    public ResponseEntity<ShippingMethod> updateShippingMethod(@RequestBody ShippingMethod shippingMethod) throws URISyntaxException {
        log.debug("REST request to update ShippingMethod : {}", shippingMethod);
        if (shippingMethod.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShippingMethod result = shippingMethodService.save(shippingMethod);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, shippingMethod.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shipping-methods} : get all the shippingMethods.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shippingMethods in body.
     */
    @GetMapping("/shipping-methods")
    public List<ShippingMethod> getAllShippingMethods() {
        log.debug("REST request to get all ShippingMethods");
        return shippingMethodService.findAll();
    }

    /**
     * {@code GET  /shipping-methods/:id} : get the "id" shippingMethod.
     *
     * @param id the id of the shippingMethod to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shippingMethod, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shipping-methods/{id}")
    public ResponseEntity<ShippingMethod> getShippingMethod(@PathVariable String id) {
        log.debug("REST request to get ShippingMethod : {}", id);
        Optional<ShippingMethod> shippingMethod = shippingMethodService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shippingMethod);
    }

    /**
     * {@code DELETE  /shipping-methods/:id} : delete the "id" shippingMethod.
     *
     * @param id the id of the shippingMethod to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shipping-methods/{id}")
    public ResponseEntity<Void> deleteShippingMethod(@PathVariable String id) {
        log.debug("REST request to delete ShippingMethod : {}", id);
        shippingMethodService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
