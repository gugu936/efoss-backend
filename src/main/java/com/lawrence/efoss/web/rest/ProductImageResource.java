package com.lawrence.efoss.web.rest;

import com.lawrence.efoss.domain.ProductImage;
import com.lawrence.efoss.repository.ProductImageRepository;
import com.lawrence.efoss.service.ProductImageService;
import com.lawrence.efoss.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing {@link com.lawrence.efoss.domain.ProductImage}.
 */
@RestController
@RequestMapping("/api")
public class ProductImageResource {

    private final Logger log = LoggerFactory.getLogger(ProductImageResource.class);

    private static final String ENTITY_NAME = "productImage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductImageService productImageService;

    private final ProductImageRepository productImageRepository;

    public ProductImageResource(ProductImageService productImageService, ProductImageRepository productImageRepository) {
        this.productImageService = productImageService;
        this.productImageRepository = productImageRepository;
    }

    /**
     * {@code POST  /product-images} : Create a new productImage.
     *
     * @param productImage the productImage to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productImage, or with status {@code 400 (Bad Request)} if the productImage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-images")
    public ResponseEntity<ProductImage> createProductImage(@RequestBody ProductImage productImage) throws URISyntaxException {
        log.debug("REST request to save ProductImage : {}", productImage);
        if (productImage.getId() != null) {
            throw new BadRequestAlertException("A new productImage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductImage result = productImageService.save(productImage);
        return ResponseEntity.created(new URI("/api/product-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-images} : Updates an existing productImage.
     *
     * @param productImage the productImage to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productImage,
     * or with status {@code 400 (Bad Request)} if the productImage is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productImage couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-images")
    public ResponseEntity<ProductImage> updateProductImage(@RequestBody ProductImage productImage) throws URISyntaxException {
        log.debug("REST request to update ProductImage : {}", productImage);
        if (productImage.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductImage result = productImageService.save(productImage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productImage.getId().toString()))
            .body(result);
    }

    @PutMapping("/product-images/reorder")
    public ResponseEntity<Map<String, String>> updateProductImageSeq(@RequestBody Map<String, Object> params) throws URISyntaxException {
        Map<String, String> result = new HashMap<>();
        List<Integer> seq = (List<Integer>) params.get("seq");
        String productId = (String) params.get("id");
        int i = 0;
        List<ProductImage> productImages = new ArrayList<>();
        for (Integer s : seq) {
           Optional<ProductImage> optionalProductImage = productImageRepository.findByProductIdAndSequence(productId, s);
           if (optionalProductImage.isPresent()) {
               ProductImage productImage = optionalProductImage.get();
               productImage.setSequence(i);
               productImages.add(productImage);
           }
           i++;
        }
        productImageRepository.saveAll(productImages);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/product-images/upload")
    public ResponseEntity<Map<String, String>> uploadProductImages(@RequestParam("files") MultipartFile[] files, String productId) throws URISyntaxException {
        Map<String, String> result = new HashMap<>();
        int i = 0;
        for (MultipartFile file : files) {
            log.debug("file {}", file.getName());
            ProductImage image = new ProductImage();
            try {
                image.sequence(i);
                image.setImage(file.getBytes());
                image.setFilename(file.getName());
                image.setImageContentType(file.getContentType());
                image.setProductId(productId);
            } catch (IOException e) {
                e.printStackTrace();
            }
            i++;
            productImageService.save(image);
        }
        // ProductImage result = productImageService.save(productImage);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/product-images/upload-one")
    public ResponseEntity<Map<String, String>> uploadProductImage(@RequestParam("file") MultipartFile file, String productId, Integer seq) {
        Map<String, String> result = new HashMap<>();
            log.debug("file {}", file.getName());
            ProductImage image = new ProductImage();
            Optional<List<ProductImage>> optionalProductImages = productImageRepository.findByProductIdOrderBySequenceAsc(productId);
            int initSeq = 0;
            if (optionalProductImages.isPresent()) {
                initSeq = optionalProductImages.get().size();
            }
            try {
                image.sequence(seq + initSeq);
                image.setImage(file.getBytes());
                image.setFilename(file.getName());
                image.setImageContentType(file.getContentType());
                image.setProductId(productId);
            } catch (IOException e) {
                e.printStackTrace();
            }
            productImageService.save(image);
        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code GET  /product-images} : get all the productImages.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productImages in body.
     */
    @GetMapping("/product-images")
    public List<ProductImage> getAllProductImages() {
        log.debug("REST request to get all ProductImages");
        return productImageService.findAll();
    }

    /**
     * {@code GET  /product-images/:id} : get the "id" productImage.
     *
     * @param id the id of the productImage to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productImage, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-images/{id}")
    public ResponseEntity<ProductImage> getProductImage(@PathVariable String id) {
        log.debug("REST request to get ProductImage : {}", id);
        Optional<ProductImage> productImage = productImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productImage);
    }

    @ResponseBody
    @RequestMapping(value = "/product-images/get-by-id/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getImageById(@PathVariable String id) throws IOException {
        byte[] images = productImageService.getImageByProductId(id);
        if (images == null) {
            return null;
        }
        InputStream in = new ByteArrayInputStream(images);
        return IOUtils.toByteArray(in);
    }

    @ResponseBody
    @RequestMapping(value = "/product-images/get-by-product-id/{id}/{seq}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getImageByProductId(@PathVariable String id, @PathVariable String seq) throws IOException {
        byte[] images = productImageService.getImageByProductId(id, seq);
        if (images == null) {
            return null;
        }
        InputStream in = new ByteArrayInputStream(images);
        return IOUtils.toByteArray(in);
    }

    @GetMapping( "/product-images/getImageCount/{id}")
    public ResponseEntity<Integer> getImageCount(@PathVariable String id) {
        Map<String,Integer> result = new HashMap<>();
        int count = 0;
        Optional<List<ProductImage>> optionalProductImages = productImageRepository.findByProductIdOrderBySequenceAsc(id);
       if (optionalProductImages.isPresent()) {
           log.debug("optionalProductImages.get().size() {}", optionalProductImages.get().size());
           count = optionalProductImages.get().size();
       }
        result.put("count", count);
       return ResponseEntity.ok().body(count);
    }

    /**
     * {@code DELETE  /product-images/:id} : delete the "id" productImage.
     *
     * @param id the id of the productImage to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-images/{id}")
    public ResponseEntity<Void> deleteProductImage(@PathVariable String id) {
        log.debug("REST request to delete ProductImage : {}", id);
        productImageService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }

    @DeleteMapping("/product-images/{id}/{seq}")
    public ResponseEntity<Void> deleteProductImageById(@PathVariable String id, @PathVariable Integer seq) {
        log.debug("REST request to delete ProductImage : {}", id);
        Optional<List<ProductImage>> optionalProductImages = productImageRepository.findByProductIdOrderBySequenceAsc(id);
        if (optionalProductImages.isPresent()) {
            Optional<ProductImage> optionalProductImage =  productImageRepository.findByProductIdAndSequence(id, seq);
            int length = seq + 1;
            optionalProductImage.ifPresent(productImage -> productImageService.delete(productImage.getId()));
            if (optionalProductImages.get().size() != length) {
                optionalProductImages = productImageRepository.findByProductIdOrderBySequenceAsc(id);
                int i = 0;
                for (ProductImage productImage : optionalProductImages.get()) {
                    productImage.setSequence(i);
                    productImageService.save(productImage);
                    i++;
                }
            }
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
