package com.lawrence.efoss.web.rest;

import com.lawrence.efoss.domain.DeliveryAddress;
import com.lawrence.efoss.service.DeliveryAddressService;
import com.lawrence.efoss.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.lawrence.efoss.domain.DeliveryAddress}.
 */
@RestController
@RequestMapping("/api")
public class DeliveryAddressResource {

    private final Logger log = LoggerFactory.getLogger(DeliveryAddressResource.class);

    private static final String ENTITY_NAME = "deliveryAddress";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeliveryAddressService deliveryAddressService;

    public DeliveryAddressResource(DeliveryAddressService deliveryAddressService) {
        this.deliveryAddressService = deliveryAddressService;
    }

    /**
     * {@code POST  /delivery-addresses} : Create a new deliveryAddress.
     *
     * @param deliveryAddress the deliveryAddress to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new deliveryAddress, or with status {@code 400 (Bad Request)} if the deliveryAddress has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/delivery-addresses")
    public ResponseEntity<DeliveryAddress> createDeliveryAddress(@RequestBody DeliveryAddress deliveryAddress) throws URISyntaxException {
        log.debug("REST request to save DeliveryAddress : {}", deliveryAddress);
        if (deliveryAddress.getId() != null) {
            throw new BadRequestAlertException("A new deliveryAddress cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DeliveryAddress result = deliveryAddressService.save(deliveryAddress);
        return ResponseEntity.created(new URI("/api/delivery-addresses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /delivery-addresses} : Updates an existing deliveryAddress.
     *
     * @param deliveryAddress the deliveryAddress to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated deliveryAddress,
     * or with status {@code 400 (Bad Request)} if the deliveryAddress is not valid,
     * or with status {@code 500 (Internal Server Error)} if the deliveryAddress couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/delivery-addresses")
    public ResponseEntity<DeliveryAddress> updateDeliveryAddress(@RequestBody DeliveryAddress deliveryAddress) throws URISyntaxException {
        log.debug("REST request to update DeliveryAddress : {}", deliveryAddress);
        if (deliveryAddress.getId() == null) {
            return createDeliveryAddress(deliveryAddress);
        }
        DeliveryAddress result = deliveryAddressService.save(deliveryAddress);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, deliveryAddress.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /delivery-addresses} : get all the deliveryAddresses.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deliveryAddresses in body.
     */
    @GetMapping("/delivery-addresses")
    public List<DeliveryAddress> getAllDeliveryAddresses() {
        log.debug("REST request to get all DeliveryAddresses");
        return deliveryAddressService.findAll();
    }

    /**
     * {@code GET  /delivery-addresses/:id} : get the "id" deliveryAddress.
     *
     * @param id the id of the deliveryAddress to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deliveryAddress, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/delivery-addresses/{id}")
    public ResponseEntity<DeliveryAddress> getDeliveryAddress(@PathVariable String id) {
        log.debug("REST request to get DeliveryAddress : {}", id);
        Optional<DeliveryAddress> deliveryAddress = deliveryAddressService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deliveryAddress);
    }

    @GetMapping("/delivery-addresses/customer")
    public ResponseEntity<DeliveryAddress> getCustomerDeliveryAddress() {
        Optional<DeliveryAddress> deliveryAddress = deliveryAddressService.getCustomerDeliveryAddress();
        return ResponseUtil.wrapOrNotFound(deliveryAddress);
    }

    /**
     * {@code DELETE  /delivery-addresses/:id} : delete the "id" deliveryAddress.
     *
     * @param id the id of the deliveryAddress to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/delivery-addresses/{id}")
    public ResponseEntity<Void> deleteDeliveryAddress(@PathVariable String id) {
        log.debug("REST request to delete DeliveryAddress : {}", id);
        deliveryAddressService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
