/**
 * View Models used by Spring MVC REST controllers.
 */
package com.lawrence.efoss.web.rest.vm;
