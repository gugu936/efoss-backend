package com.lawrence.efoss.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

/**
 * A CustomerOrder.
 */
@Document(collection = "customer_order")
public class CustomerOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("cart_id")
    private String cartId;

    @Field("user_id")
    private String userId;

    @Field("status")
    private String status;

    @Field("order_ids")
    private Map<String, List<String>> orderIds;

    @Field("delivery_address_id")
    private String deliveryAddressId;

    @Field("billing_address_id")
    private String billingAddressId;

    @Field("payment_method_id")
    private String paymentMethodId;

    @Field("shipping_method_id")
    private String shippingMethodId;

    @Field("same_as_billing")
    private Boolean sameAsBilling;

    @Field("discount")
    private BigDecimal discount;

    @Field("total")
    private BigDecimal total;

    @Field("created_at")
    private ZonedDateTime createdAt;

    @Field("updated_at")
    private ZonedDateTime updatedAt;

    @Field("use_e_wallet")
    private Boolean useEWallet;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCartId() {
        return cartId;
    }

    public CustomerOrder cartId(String cartId) {
        this.cartId = cartId;
        return this;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getStatus() {
        return status;
    }

    public CustomerOrder status(String status) {
        this.status = status;
        return this;
    }

    public CustomerOrder useEWallet(Boolean useEWallet) {
        this.useEWallet = useEWallet;
        return this;
    }


    public String getShippingMethodId() {
        return shippingMethodId;
    }

    public Boolean getUseEWallet() {
        return useEWallet;
    }

    public void setUseEWallet(Boolean useEWallet) {
        this.useEWallet = useEWallet;
    }

    public void setShippingMethodId(String shippingMethodId) {
        this.shippingMethodId = shippingMethodId;
    }

    public CustomerOrder shippingMethodId(String shippingMethodId) {
        this.shippingMethodId = shippingMethodId;
        return this;
    }

    public CustomerOrder sameAsBilling(Boolean sameAsBilling) {
        this.sameAsBilling = sameAsBilling;
        return this;
    }

    public Boolean getSameAsBilling() {
        return sameAsBilling;
    }

    public void setSameAsBilling(Boolean sameAsBilling) {
        this.sameAsBilling = sameAsBilling;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Map<String, List<String>> getOrderIds() {
        return orderIds;
    }

    public String getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(String deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public String getBillingAddressId() {
        return billingAddressId;
    }

    public CustomerOrder billingAddressId(String billingAddressId) {
        this.billingAddressId = billingAddressId;
        return this;
    }

    public CustomerOrder deliveryAddressId(String deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
        return this;
    }

    public void setBillingAddressId(String billingAddressId) {
        this.billingAddressId = billingAddressId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public CustomerOrder userId(String userId) {
        this.userId = userId;
        return this;
    }

    public CustomerOrder orderIds(Map<String, List<String>> orderId) {
        this.orderIds = orderId;
        return this;
    }

    public void setOrderIds(Map<String, List<String>> orderId) {
        this.orderIds = orderId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public CustomerOrder paymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
        return this;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public CustomerOrder discount(BigDecimal discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public CustomerOrder total(BigDecimal total) {
        this.total = total;
        return this;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public CustomerOrder createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public CustomerOrder updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomerOrder)) {
            return false;
        }
        return id != null && id.equals(((CustomerOrder) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CustomerOrder{" +
            "id=" + getId() +
            ", cartId='" + getCartId() + "'" +
            ", status='" + getStatus() + "'" +
            ", orderIds='" + getOrderIds() + "'" +
            ", paymentMethodId='" + getPaymentMethodId() + "'" +
            ", discount=" + getDiscount() +
            ", total=" + getTotal() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
